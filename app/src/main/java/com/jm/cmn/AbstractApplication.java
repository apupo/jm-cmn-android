package com.jm.cmn;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.jm.cmn.interfaces.IApplication;
import com.jm.cmn.interfaces.IService;
import com.jm.cmn.interfaces.IServiceProvider;
import com.jm.cmn.interfaces.IStore;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.managers.SyncManager;
import com.jm.cmn.utils.SharedPreferences;
import com.jm.cmn.views.activities.AbstractActivity;

import java.util.HashMap;

/**
 * Created by Jakub Muran on 25.05.2016.
 */
public abstract class AbstractApplication extends Application implements IApplication, IStoreProvider, IServiceProvider {

    private HashMap<Class<?>, IStore> mStores;
    private HashMap<Class<?>, IService> mServices;

    protected abstract String makeSharedPreferencesKey();

    @Override
    public void onSyncStart() {
        if(getSyncManager() == null) return;
        getSyncManager().performSync();
    }

    @Override
    public void onSyncStop() {
        if(getSyncManager() == null) return;
        getSyncManager().stopSync();
    }

    @Override
    public void onSyncStart(SyncManager.OnSyncFinishedCallback callback) {
        if(getSyncManager() == null) return;
        getSyncManager().performSync(callback);
    }

    @Override
    public SyncManager getSyncManager() {
        return null;
    }

    private AbstractActivity mCurrentActivity;
    @Override
    public AbstractActivity getContext() {
        return mCurrentActivity;
    }

    @Override
    public void onActivityStart(AbstractActivity activity) {
        mCurrentActivity = activity;
    }

    @Override
    public void onActivityStop(AbstractActivity activity) {}

    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        mStores = new HashMap<Class<?>, IStore>();
        mServices = new HashMap<Class<?>, IService>();

        SharedPreferences.init(getApplicationContext(), makeSharedPreferencesKey() != null ? makeSharedPreferencesKey() : getClass().getSimpleName());
    }

    @Override
    public <T extends IStore> T getStore(Class<T> cls) {
        //lazy load
        IStore store = mStores.get(cls);
        if(store == null) {
            store = populateStore(cls);
            if(store != null) mStores.put(cls, store);
        }
        return (T) store;
    }

    private <T extends IStore> T populateStore(Class<T> cls) {
        Class[] args = new Class[1];
        args[0] = IStoreProvider.class;
        IStore store = null;
        try {
            store = (T) cls.getDeclaredConstructor(args).newInstance(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) store;
    }

    @Override
    public <T extends IService> T getService(Class<T> cls) {
        IService service = mServices.get(cls);
        if(service == null) {
            service = populateService(cls);
            if(service != null) mServices.put(cls, service);
        }
        return (T) service;
    }

    private <T extends IService> T populateService(Class<T> cls) {
        Class[] args = new Class[1];
        args[0] = IServiceProvider.class;
        IService service = null;
        try {
            service = (T) cls.getDeclaredConstructor(args).newInstance(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (T) service;
    }

}
