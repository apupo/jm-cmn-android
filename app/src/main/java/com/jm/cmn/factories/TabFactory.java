package com.jm.cmn.factories;

import android.content.Context;
import android.view.View;
import android.widget.TabHost;

/**
 * Created by muran on 22.6.2015.
 */
public class TabFactory implements TabHost.TabContentFactory {

    private Context context;

    public TabFactory(Context context) {
        this.context = context;
    }

    public View createTabContent(String tag) {
        View v = new View(context);
        v.setMinimumWidth(0);
        v.setMinimumHeight(0);
        return v;
    }

}
