package com.jm.cmn;

import android.content.Context;

/**
 * Created by Jakub Muran on 20.02.2017.
 */
public class Config {

    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    public static String getEndPoint() {
        if(mContext == null) return "";
        return mContext.getResources().getString(R.string.network_end_point);
    }

}
