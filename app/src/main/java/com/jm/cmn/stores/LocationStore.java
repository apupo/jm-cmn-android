package com.jm.cmn.stores;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.jm.cmn.AbstractApplication;
import com.jm.cmn.interfaces.IStoreProvider;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jakub Muran on 06.03.2017.
 */

public class LocationStore extends AbstractStore implements LocationListener {

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    public static final int PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE = 5;

    private Location mCurrentBestLocation;
    protected Location getCurrentBestLocation() {
        return mCurrentBestLocation;
    }

    private Context mContext;
    protected Context getContext() {
        return mContext;
    }

    private boolean mEnable;
    public boolean isEnabled() { return mEnable; };
    public void setEnable(boolean enable) { mEnable = enable; }

    private Map<Object, OnLocationListener> mLocationListenersMap;

    public void registerLocationListener(Object delegate, OnLocationListener locationListener) {
        if(mLocationListenersMap.containsKey(delegate)) return;
        mLocationListenersMap.put(delegate, locationListener);
    }

    public void unregisterLocationListener(Object delegate) {
        if(!mLocationListenersMap.containsKey(delegate)) return;
        mLocationListenersMap.remove(delegate);
    }

    private LocationManager mLocationManager;
    protected LocationManager getLocationManager() {
        return mLocationManager;
    }

    private AbstractApplication mApplication;
    protected AbstractApplication getApplication() {
        return mApplication;
    }

    public LocationStore(IStoreProvider storeProvider) {
        super(storeProvider);
        mLocationListenersMap = new HashMap<>();
        mContext = (Context) storeProvider;
        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        mApplication = (AbstractApplication) storeProvider;
    }


    public void requestLocationUpdate() {
        if(!isEnabled()) return;
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getApplication().getContext(), new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE);
        } else {
            getLocationManager().requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            getLocationManager().requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    public void stopProvidingLocation() {
        if(!isEnabled()) return;
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getApplication().getContext(), new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE);
        } else {
            getLocationManager().removeUpdates(this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(isBetterLocation(location, getCurrentBestLocation())) {
            mCurrentBestLocation = location;

            HashMap<Object, OnLocationListener> copy = new HashMap<Object, OnLocationListener>(mLocationListenersMap);
            for (OnLocationListener listener : copy.values()) {
                if(listener != null) listener.onLocationChanged(mCurrentBestLocation);
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if(currentBestLocation == null) { // A new location is always better than no location :D
            return true;
        }

        // Check whether the new location is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if(isSignificantlyNewer) {
            return true;
        //if the new location is more than two minutes older, it must be worse
        } else if(isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location is more or les accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        if(isMoreAccurate) {
            return true;
        } else if(isNewer && !isLessAccurate) {
            return true;
        } else if(isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }

        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if(provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public interface OnLocationListener {
        public void onLocationChanged(Location location);
    }

}
