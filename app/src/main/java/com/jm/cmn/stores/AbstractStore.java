package com.jm.cmn.stores;

import com.jm.cmn.interfaces.IServiceProvider;
import com.jm.cmn.interfaces.IStore;
import com.jm.cmn.interfaces.IStoreProvider;

/**
 * Created by muran on 16.6.2015.
 */
public class AbstractStore implements IStore {

    private IServiceProvider serviceProvider;
    @Override
    public IServiceProvider getServiceProvider() {
        return this.serviceProvider;
    }

    private IStoreProvider storeProvider;
    @Override
    public IStoreProvider getStoreProvider() {
        return this.storeProvider;
    }

    public AbstractStore(final IStoreProvider storeProvider) {
        this.storeProvider = storeProvider;
        this.serviceProvider = (IServiceProvider) storeProvider;
    }
}
