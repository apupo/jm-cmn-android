package com.jm.cmn.interfaces;

/**
 * Created by muran on 25.6.2015.
 */
public interface ICameraCallback {
    public void onCameraResult(Object object);
}
