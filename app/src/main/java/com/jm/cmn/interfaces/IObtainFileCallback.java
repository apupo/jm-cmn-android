package com.jm.cmn.interfaces;

import android.content.Intent;

/**
 * Created by Jakub Muran on 16.10.2017.
 */

public interface IObtainFileCallback {
    void onFileObtainResult(Intent data);
}
