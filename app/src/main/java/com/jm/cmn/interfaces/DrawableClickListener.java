package com.jm.cmn.interfaces;

import com.jm.cmn.enums.DrawablePosition;

/**
 * Created by Jakub Muran on 03.10.2017.
 */

public interface DrawableClickListener {
    public void onDrawableClick(DrawablePosition position);
}
