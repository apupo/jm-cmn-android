package com.jm.cmn.interfaces;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v7.app.ActionBar;

import com.jm.cmn.models.AlertModel;
import com.jm.cmn.views.fragments.AbstractDialogFragment;

/**
 * Created by muran on 7/23/2015.
 */
public interface IAbstractView {
    public void showAlert(AlertModel alert);
    public void showDialog(AbstractDialogFragment dialog);
    public AbstractDialogFragment getCurrentDialog();
    public SharedPreferences getSharedPreferences();
    public ViewDataBinding getViewDataBinding();
    public Resources getResources();
    public Context getContext();
    public ActionBar getSupportActionBar();
    public void navigateTo(Class<?> cls);
    public void navigateTo(Class<?> cls, Bundle args);
    public void navigateTo(Class<?> cls, boolean addToBackStack);
    public void navigateTo(Class<?> cls, Bundle args, boolean addToBackStack);
    public void navigateToBack();
    public void navigateToBack(Bundle args);
}
