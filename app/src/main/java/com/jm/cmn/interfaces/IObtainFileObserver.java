package com.jm.cmn.interfaces;

/**
 * Created by Jakub Muran on 16.10.2017.
 */
public interface IObtainFileObserver {
    void obtainFile(IObtainFileCallback callback);
}
