package com.jm.cmn.interfaces;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.jm.cmn.models.NavigationModel;
import com.jm.cmn.viewmodels.NavigationViewModel;

/**
 * Created by Jakub Muran on 5.6.2015.
 */
public interface INavigationView extends IAbstractView {
    public NavigationViewModel getNavigationViewModel();
    public void onNavigateNotify(NavigationModel navigate, Bundle params);
    public void setNavigationLocked(boolean locked);
    public Toolbar getToolbar();
    public void hideNavigationMenu();
    public void showNavigationMenu();
    public void refresh();
}
