package com.jm.cmn.interfaces;

import com.jm.cmn.models.AlertModel;

/**
 * Created by Jakub Muran on 30.05.2016.
 */
public interface IAlertCallback {
    public void onCloseEvent(AlertModel model);
}
