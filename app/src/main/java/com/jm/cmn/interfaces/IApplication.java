package com.jm.cmn.interfaces;

import android.content.Context;

import com.jm.cmn.managers.SyncManager;
import com.jm.cmn.views.activities.AbstractActivity;

/**
 * Created by Jakub Muran on 26.05.2016.
 */
public interface IApplication {
    public void onActivityStart(AbstractActivity activity);
    public void onActivityStop(AbstractActivity activity);
    public SyncManager getSyncManager();
    public void onSyncStart(SyncManager.OnSyncFinishedCallback callback);
    public void onSyncStart();
    public void onSyncStop();
    public Context getContext();
    public Context getApplicationContext();
}
