package com.jm.cmn.interfaces;

/**
 * Created by blackie on 23.7.2015.
 */
public interface IStoreProvider {
    public <T extends IStore> T getStore(Class<T> cls);
}
