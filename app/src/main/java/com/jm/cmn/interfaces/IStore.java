package com.jm.cmn.interfaces;

/**
 * Created by muran on 16.6.2015.
 */
public interface IStore {
    public IStoreProvider getStoreProvider();
    public IServiceProvider getServiceProvider();
}
