package com.jm.cmn.interfaces;

/**
 * Created by muran on 8/17/2015.
 */
public interface IServiceProvider {
    public <T extends IService> T getService(Class<T> cls);
}
