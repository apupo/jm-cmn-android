package com.jm.cmn.interfaces;

/**
 * Created by Jakub Muran on 31.03.2017.
 */
public interface IImageRecyclerData extends IAbstractRecyclerViewRendererData {
    public String getImagePath();
    public boolean isDirty();
}
