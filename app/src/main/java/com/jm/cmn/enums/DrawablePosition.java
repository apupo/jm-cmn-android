package com.jm.cmn.enums;

/**
 * Created by Jakub Muran on 03.10.2017.
 */

public enum DrawablePosition {
    LEFT, RIGHT, TOP, BOTTOM;
}
