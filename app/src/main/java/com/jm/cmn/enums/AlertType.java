package com.jm.cmn.enums;

/**
 * Created by muran on 7/23/2015.
 */
public enum AlertType {
    OK, CANCEL, OK_CANCEL, RELOAD;
}
