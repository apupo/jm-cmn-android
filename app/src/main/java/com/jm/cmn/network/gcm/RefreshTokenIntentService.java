package com.jm.cmn.network.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by Jakub Muran on 10/29/2017.
 */

public abstract class RefreshTokenIntentService extends InstanceIDListenerService {

    private static final String TAG = "InstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        final Intent intent = new Intent(this, getRegistrationIntetServiceClass());
        startService(intent);
    }
    // [END refresh_token]

    abstract protected Class<? extends RegistrationIntentService> getRegistrationIntetServiceClass();
}
