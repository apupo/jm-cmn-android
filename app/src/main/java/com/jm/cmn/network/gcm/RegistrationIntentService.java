package com.jm.cmn.network.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * Created by Jakub Muran on 10/29/2017.
 */

public abstract class RegistrationIntentService extends IntentService {

    public static class Preferences {
        private static final String SENT_TOKEN_TO_SERVER = "sent_token_to_server";
        public static final String REGISTRATION_COMPLETE = "registration_complete";
    }

    public interface OnRegistrationTokenToServerListener {
        void onSuccess();
        void onFailed(Exception error);
    }

    private static final String TAG = "RegDeviceToken";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // [START register_gcm_device_token]
            // Initially this call goes out to the network to retrieve the token, subsequent calls are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // [START get_token]
            final InstanceID instanceID = InstanceID.getInstance(this);
            final int senderId = getResources().getIdentifier("gcm_defaultSenderId", "string", getPackageName());

            if(senderId == 0) {
                Log.d(TAG, "Missing gcm_defaultSenderId.");
                return;
            } else {
                final String token = instanceID.getToken(getString(senderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.i(TAG, "GCM Registration token " + token);
                // [END get_token]
                if(!TextUtils.isEmpty(token)) sendRegistrationToServer(token, new OnRegistrationTokenToServerListener() {
                    @Override
                    public void onSuccess() {
                        // You should store a boolean that indicates whether the generated token has been
                        // sent to your server. If the boolean is false, send the token to your server,
                        // otherwise your server should have already received the token.
                        sharedPreferences.edit().putBoolean(Preferences.SENT_TOKEN_TO_SERVER, true).apply();
                        // [END register_for_gcm]
                        // Notify UI that registration has completed, so the progress indicator can be hidden.
                        Intent registrationComplete = new Intent(Preferences.REGISTRATION_COMPLETE);
                        LocalBroadcastManager.getInstance(RegistrationIntentService.this).sendBroadcast(registrationComplete);
                    }

                    @Override
                    public void onFailed(Exception error) {
                        failedToCompleteTokenRefresh(sharedPreferences, error);
                    }
                });
            }

        } catch (Exception e) {
            failedToCompleteTokenRefresh(sharedPreferences, e);
        }
    }

    private void failedToCompleteTokenRefresh(final SharedPreferences sharedPreferences, Exception error) {
        Log.d(TAG, "Failed to complete token refresh", error);
        // If an exception happens while fetching the new token or updating our registration data
        // on a third-party server, this ensures that we'll attempt the update at a later time.
        sharedPreferences.edit().putBoolean(Preferences.SENT_TOKEN_TO_SERVER, false).apply();
    }

    abstract protected void sendRegistrationToServer(final String token, final OnRegistrationTokenToServerListener listener);
}
