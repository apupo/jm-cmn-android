package com.jm.cmn.network;

import com.google.gson.Gson;
import com.jakewharton.retrofit.Ok3Client;
import com.jm.cmn.interfaces.IService;
import com.jm.cmn.interfaces.IServiceProvider;

import okhttp3.OkHttpClient;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

/**
 * Created by muran on 1/15/2016.
 */
public abstract class AbstractNetworkService implements IService {

    private IServiceProvider serviceProvider;
    protected IServiceProvider getServiceProvider() { return this.serviceProvider; }

    private RestAdapter adapter;
    protected RestAdapter getAdapter() {
        if(this.adapter == null) {
            this.adapter = getBuilder().build();
        }
        return adapter;
    }

    private RestAdapter.Builder builder;
    protected RestAdapter.Builder getBuilder() {
        if(this.builder == null) {
            this.builder = createBuilder();
        }
        return this.builder;
    }

    public AbstractNetworkService(IServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public void restart() {
        builder = createBuilder();
    }

    private RestAdapter.Builder createBuilder() {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(getEndpoint())
                .setClient(new Ok3Client(getOkHttpClient()))
                .setConverter(getConverter());
    }

    public  <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null, null);
    }

    public  <S> S createService(Class<S> serviceClass, final String tokenKey, final String tokenValue) {
        addHeader(tokenKey, tokenValue);
        getBuilder().setErrorHandler(errorHandler);
        return getAdapter().create(serviceClass);
    }

    protected void addHeader(final String key, final String value) {
        if(key != null && value != null) {
            getBuilder().setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader(key, value);
                }
            });
        }
    }

    private ErrorHandler errorHandler = new ErrorHandler() {
        @Override
        public Throwable handleError(RetrofitError cause) {
            return handleRetrofitError(cause);
        }
    };

    protected Throwable handleRetrofitError(RetrofitError error) {
        return error;
    }

    protected void onErrorOccured(RetrofitError cause) {

    }

    protected abstract OkHttpClient getOkHttpClient();
    protected abstract String getEndpoint();
    protected Converter getConverter() {
        return new GsonConverter(new Gson());
    }

}
