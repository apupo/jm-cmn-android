package com.jm.cmn.network;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Jakub Muran on 06.02.2017.
 */
public class CancelableNetworkCallback<T> implements Callback<T> {

    private Callback<T> mCallback;

    private boolean mCanceled;

    private CancelableNetworkCallback() {}

    public CancelableNetworkCallback(Callback<T> callback) {
        mCallback = callback;
        mCanceled = false;
    }

    public void cancel() {
        mCanceled = true;
    }

    @Override
    public void success(T t, Response response) {
        if(!mCanceled) {
            mCallback.success(t, response);
        } else {
            //cancel true
            mCallback.success(t, null);
            mCallback = null;
        }
    }

    @Override
    public void failure(RetrofitError error) {
        if(!mCanceled) {
            mCallback.failure(error);
        }
    }
}
