package com.jm.cmn.network;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.jm.cmn.utils.SharedPreferences;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Jakub Muran on 13.04.2017.
 */
abstract public class AuthenticationInterceptor implements Interceptor {

    public static final String COOKIES_PREF_KEY                     =   "cookies_pref_key";

    private static final String LOG_TAG                             =   "NetworkInterceptor";

    private static final String DEFAULT_COOKIE_KEY                  =   "Cookie";
    private static final String DEFAULT_SESSION_TOKEN_KEY           =   "Set-Cookie";

    private static final String DEFAULT_AUTH_TOKEN_BODY_KEY         =   "token";
    private static final String DEFAULT_AUTH_TOKEN_KEY              =   "AuthToken";
    private static final String AUTH_TOKEN_PREF_KEY                 =   "auth_token_pref_key";

    abstract protected String getAuthTokenKey();

    abstract protected String getAuthTokenBodyKey();

    abstract protected String getCookieKey();

    abstract protected String getSessionTokenHeaderKey();

    abstract protected String getEndpoint();

    abstract protected String getRelativeLoginPath();

    abstract protected Object getAuthBody();

    protected RequestBody getAuthRequestBody() {
        Gson gson = new Gson();
        String body = gson.toJson(getAuthBody());
        return RequestBody.create(MediaType.parse("application/json"), body);
    }

    protected void applyAuthTokenFromPersistanceStore(Request.Builder builder) {
        final String authToken = SharedPreferences.getDefaultPreferences().getString(AUTH_TOKEN_PREF_KEY, null);
        if(!TextUtils.isEmpty(authToken)) {
            final String authTokenKey = getAuthTokenKey() == null ? DEFAULT_AUTH_TOKEN_KEY : getAuthTokenKey();
            builder.addHeader(authTokenKey, authToken);
            Log.d(LOG_TAG, "Read auth token = " + authToken);
        }
    }

    protected void applyCookiesFromPersistanceStore(Request.Builder builder) {
        HashSet<String> preferences = (HashSet) SharedPreferences.getDefaultPreferences().getStringSet(COOKIES_PREF_KEY, new HashSet<String>());
        for (String cookie : preferences) {
            builder.addHeader(getCookieKey() == null ? DEFAULT_COOKIE_KEY : getCookieKey(), cookie);
        }
        Log.d(LOG_TAG, "Apply cookies from prefs.");
        Log.d(LOG_TAG, preferences.toString());
    }

    protected void storeAuthToken(Response response) {
        final String authTokenKey = getAuthTokenKey() == null ? DEFAULT_AUTH_TOKEN_KEY : getAuthTokenKey();
        String authToken = response.header(authTokenKey);

        if(authToken == null) { //try from body
            final Gson gson = new Gson();
            try {
                Object body = gson.fromJson(response.body().charStream(), Object.class);
                if(body != null) {
                    final JSONObject jsonObject = new JSONObject(body.toString());
                    if(jsonObject != null) {
                        final String tokenKey = getAuthTokenBodyKey() == null ? DEFAULT_AUTH_TOKEN_BODY_KEY : getAuthTokenBodyKey();
                        if(jsonObject.has(tokenKey)) {
                            authToken = jsonObject.getString(tokenKey);
                        }
                    }
                }
            } catch (Exception e) {}
        }

        storeAuthToken(authToken);

    }

    public static void storeAuthToken(final String authToken) {
        if (authToken != null) {
            Log.d(LOG_TAG, "STORE AUTH TOKEN = " + authToken);
            SharedPreferences.getDefaultPreferences().edit().putString(AUTH_TOKEN_PREF_KEY, authToken).apply();
        }
    }

    protected void storeCookies(Response response) {
        final String sessionTokenKey = getSessionTokenHeaderKey() == null ? DEFAULT_SESSION_TOKEN_KEY : getSessionTokenHeaderKey();
        if(!response.headers(sessionTokenKey).isEmpty()) {
            HashSet<String> cookies = new HashSet<>();

            for (String header : response.headers(sessionTokenKey)) {
                cookies.add(header);
            }

            SharedPreferences.getDefaultPreferences().edit()
                    .putStringSet(COOKIES_PREF_KEY, cookies)
                    .apply();

            Log.d(LOG_TAG, "Store cookies to prefs.");
            Log.d(LOG_TAG, cookies.toString());
        }
    }

    protected Request makeAuthRequest(final Headers headers) {
        if(getEndpoint() == null) return null;
        Request.Builder authRequestBuilder = new Request.Builder();
        StringBuilder urlStringBuilder = new StringBuilder(getEndpoint());
        urlStringBuilder.append(getRelativeLoginPath());
        authRequestBuilder.url(urlStringBuilder.toString());
        authRequestBuilder.method("POST", getAuthRequestBody());
        authRequestBuilder.headers(headers);
        return authRequestBuilder.build();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Log.d(LOG_TAG, "Original request: " + originalRequest.toString());

        Request.Builder originalRequestBuilder = originalRequest.newBuilder();
        //applyCookiesFromPersistanceStore(originalRequestBuilder);
        applyAuthTokenFromPersistanceStore(originalRequestBuilder);

        Response originalResponse = chain.proceed(originalRequestBuilder.build());
        Log.d(LOG_TAG, "Original response: " + originalResponse.toString());

        if(originalResponse.message().equalsIgnoreCase("Unauthorized")) {
            Log.d(LOG_TAG, "Unauthorized we need try re-auth.");
            Request authRequest = makeAuthRequest(originalRequest.headers());
            Log.d(LOG_TAG, authRequest.toString());
            Response authResponse = chain.proceed(authRequest);
            Log.d(LOG_TAG, authResponse.toString());
            //storeCookies(authResponse);
            storeAuthToken(authResponse);
            //applyCookiesFromPersistanceStore(originalRequestBuilder);
            applyAuthTokenFromPersistanceStore(originalRequestBuilder);
            originalResponse = chain.proceed(originalRequestBuilder.build());
            Log.d(LOG_TAG, originalResponse.toString());
        } else if(originalResponse.code() == 200 && originalRequest.url().toString().contains("login")) {
            //storeCookies(originalResponse);
            storeAuthToken(chain.proceed(originalRequestBuilder.build()));
        }

        return originalResponse;
    }
}
