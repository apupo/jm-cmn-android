package com.jm.cmn.models;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by Jakub Muran on 22.6.2015.
 */
public class TabInfoModel {
    private String tag;
    public String getTag() { return tag; }

    private Class<?> cls;
    private Bundle args;
    private Fragment fragment;

    public TabInfoModel(String tag, Class<?> cls, Bundle args) {
        this.tag = tag;
        this.cls = cls;
        this.args = args;
    }

}
