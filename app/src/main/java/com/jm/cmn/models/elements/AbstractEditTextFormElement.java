package com.jm.cmn.models.elements;

/**
 * Created by Jakub Muran on 14.02.2017.
 * CoverPage s.r.o.
 */

public abstract class AbstractEditTextFormElement extends AbstractComponent {

    protected abstract int getTextBindingVariable();
    protected abstract int getHintBindingVariable();

    private String text;
    public String getText() {
        return this.text;
    }

    private OnTextChangedListener mOnTextChangedListener;
    public void setOnTextChangedListener(OnTextChangedListener listener) {
        mOnTextChangedListener = listener;
    }

    public void setText(String text) {
        if(mOnTextChangedListener != null) mOnTextChangedListener.onTextChanged(this.text, text);
        this.text = text;
    }

    public void clear() {
        this.text = null;
    }

    private int hint;
    public int getHint() { return this.hint; }
    public void setHint(int hint) {
        this.hint = hint;
    }

    public AbstractEditTextFormElement(int titleResID) {
        super(titleResID);
    }

    public AbstractEditTextFormElement(int titleResID, int hintResID) {
        super(titleResID);
        setHint(hintResID);
    }

    public AbstractEditTextFormElement(int titleResID, String text, int hint) {
        super(titleResID);
        setText(text);
        setHint(hint);
    }

    public interface OnTextChangedListener {
        public void onTextChanged(String old, String current);
    }

    @Override
    protected int validate() {
        if(getText() == null) return -1;
        if(getText().isEmpty()) return -1;
        return super.validate();
    }
}
