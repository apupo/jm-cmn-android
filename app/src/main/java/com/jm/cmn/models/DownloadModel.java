package com.jm.cmn.models;

import android.databinding.BaseObservable;

/**
 * Created by Jakub Muran on 11/8/2017.
 */

public class DownloadModel extends BaseObservable {

    public enum State {
        IDLE, WAITING, DOWNLOADING, FINISHED;
    }

    private State state = State.IDLE;
    public State getState() { return state; }
    public void setState(State state) {
        this.state = state;
        notifyChange();
    }

    private String authorization;
    public String getAuthorization() { return authorization; }
    public void setAuthorization(String authorization) { this.authorization = authorization; }

    private String filename;
    public String getFilename() { return filename; }

    private String filePath;
    public String getFilePath() { return filePath; }

    private String downloadedFilePath;
    public String getDownloadedFilePath() { return downloadedFilePath; }
    public void setDownloadedFilePath(String downloadedFilePath) { this.downloadedFilePath = downloadedFilePath; }

    private int progress;
    public int getProgress() { return progress; }
    public void setProgress(int progress) { this.progress = progress; }

    private int currentFileSize;
    public int getCurrentFileSize() { return currentFileSize; }
    public void setCurrentFileSize(int currentFileSize) { this.currentFileSize = currentFileSize; }

    private int totalFileSize;
    public int getTotalFileSize() { return totalFileSize; }
    public void setTotalFileSize(int totalFileSize) { this.totalFileSize = totalFileSize; }

    public DownloadModel(String filename, String filePath) {
        this.filename = filename;
        this.filePath = filePath;
    }
}
