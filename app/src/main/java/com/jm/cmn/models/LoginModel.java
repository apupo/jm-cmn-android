package com.jm.cmn.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jakub Muran on 21.02.2017.
 */
public class LoginModel {

    public static final String SERIALIZED_CLASS_NAME = "LoginModel";

    @SerializedName("Pin")
    public String pin;

    public LoginModel() {
    }

    public LoginModel(String pin) {
        this.pin = pin;
    }
}
