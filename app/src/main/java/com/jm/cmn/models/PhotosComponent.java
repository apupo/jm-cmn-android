package com.jm.cmn.models;

import android.view.View;

import com.jm.cmn.adapters.AbstractRecyclerAdapter;
import com.jm.cmn.adapters.PhotosRecyclerAdapter;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.ICameraCallback;
import com.jm.cmn.interfaces.ICameraView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 23.02.2017.
 */
public class PhotosComponent extends AbstractRecyclerViewComponent implements ICameraCallback {

    public interface OnPhotosChangedListener {
        void onPhotosChanged(List<IAbstractRecyclerViewRendererData> photos, Photo added, Photo removed);
    }

    private ICameraView mCameraView;

    private OnPhotosChangedListener mPhotosChangedListener;
    public void setOnPhotosChangedListener(OnPhotosChangedListener listener) { this.mPhotosChangedListener = listener; }

    public PhotosComponent(int titleResID, int orientation, AbstractRecyclerAdapter adapter, List<IAbstractRecyclerViewRendererData> data, IAbstractView abstractView) {
        super(titleResID, orientation, adapter, data, abstractView);
        this.mCameraView = (ICameraView) abstractView;
        ((PhotosRecyclerAdapter) adapter).setComponent(this);
    }

    public void onButtonClick(View view) {
        if(this.mCameraView == null) return;
        this.mCameraView.obtainPhoto(this);
    }

    @Override
    public void onCameraResult(Object object) {
        if(object != null && object instanceof String) {
            Photo photo = new Photo();
            photo.setUrl(String.valueOf(object));
            photo.deletable = true;

            add(photo);

            dispatchOnPhotosChanged(photo, null);
        }
    }

    public void dispatchOnPhotosChanged(Photo added, Photo removed) {
        if(mPhotosChangedListener != null) {
            mPhotosChangedListener.onPhotosChanged(new ArrayList<>(getAdapter().getSource()), added, removed);
        }
    }
}
