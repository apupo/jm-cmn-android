package com.jm.cmn.models;

import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.views.fragments.AbstractFragment;

import org.parceler.Parcel;

/**
 * Created by Jakub Muran on 5.6.2015.
 */
@Parcel
public class NavigationModel implements IAbstractRecyclerViewRendererData {

    public static final String BUNDLE_KEY       =   "NAVIGATION_MODEL_BUNDLE_KEY";
    public static final String PARCELABLE_KEY   =   "NAVIGATION_MODEL_PARCELABLE_KEY";
    public static final String ARG_BUNDLE_KEY   =   "NAVIGATION_MODEL_ARGUMENTS_BUNDLE_KEY";

    private boolean selected;
    public boolean isSelected() { return selected; }
    public void setSelected(boolean selected) { this.selected = selected; }

    private boolean navigateViaBroadcast;
    public boolean isNavigateViaBroadcast() { return navigateViaBroadcast; }

    private Class<? extends AbstractFragment> navigationFragmentClass;
    public Class<? extends AbstractFragment> getNavigationFragmentClass() { return navigationFragmentClass; }

    private boolean stepBack;
    public boolean isStepBack() { return stepBack; }
    public void resetStepBack() { this.stepBack = false; }

    private boolean rewind;
    public boolean isRewind() { return rewind; }
    public void setRewind(boolean rewind) { this.rewind = rewind; }
    public void resetRewind() { this.rewind = false; }

    private String title;
    public String getTitle() { return title; }

    private int icon;
    public int getIcon() { return icon; }
    public void setIcon(int icon) { this.icon = icon; }

    public int badgeCount;

    public NavigationModel() {

    }

    public NavigationModel(Class<? extends AbstractFragment> navigationFragmentClass) {
        this.navigationFragmentClass = navigationFragmentClass;
    }

    public NavigationModel(String title, int icon, Class<? extends AbstractFragment> navigationFragmentClass) {
        this(navigationFragmentClass);
        this.title = title;
        this.icon = icon;
    }

    public NavigationModel(String title, int icon, Class<? extends AbstractFragment> navigationFragmentClass, boolean navigateViaBroadcast) {
        this(title, icon, navigationFragmentClass);
        this.navigateViaBroadcast = navigateViaBroadcast;
    }

    public AbstractFragment createFragment() {
        try {
            return getNavigationFragmentClass().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
