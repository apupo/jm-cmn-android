package com.jm.cmn.models;

import com.jm.cmn.enums.AlertAction;
import com.jm.cmn.enums.AlertType;
import com.jm.cmn.interfaces.IAlertCallback;

/**
 * Created by muran on 3/22/2016.
 */
public class AlertModel {

    public static final String PARCELABLE_KEY = "alert_model_parcelable";

    public AlertType type;
    public AlertAction action = null;
    public IAlertCallback callback;
    public String title;
    public String message;

    public AlertModel() {
    }

    public AlertModel(AlertType type, String title, String message) {
        this.type = type;
        this.title = title;
        this.message = message;
    }

    public AlertModel(AlertType type, String title, String message, IAlertCallback callback) {
        this(type, title, message);
        this.callback = callback;
    }
}
