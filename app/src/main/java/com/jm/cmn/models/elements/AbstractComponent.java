package com.jm.cmn.models.elements;

import android.content.Context;
import android.databinding.BaseObservable;

/**
 * Created by Jakub Muran on 14.02.2017.
 */
public abstract class AbstractComponent extends BaseObservable {

    private boolean visible = true;
    public boolean getVisible() {
        return this.visible;
    }
    public void setVisible(boolean visible) {
        if(this.visible == visible) return;
        this.visible = visible;
    }

    private int titleResID;
    public int getTitleResID() {
        return this.titleResID;
    }
    public void setTitleResID(int titleResID) {
        this.titleResID = titleResID;
    }

    private boolean enabled = true;
    public boolean isEnabled() {
        return this.enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public AbstractComponent(int titleResID) {
        setTitleResID(titleResID);
    }

    private int invalidMessageResID;
    public int getInvalidMessageResID() {
        return this.invalidMessageResID;
    }
    public void setInvalidMessageResID(int invalidMessageResID) {
        this.invalidMessageResID = invalidMessageResID;
    }

    private void resetValidation() {
        setInvalidMessageResID(0);
        this.valid = false;
    }

    private boolean valid;
    public boolean isValid() {
        resetValidation();
        setInvalidMessageResID(validate());
        this.valid = this.invalidMessageResID == 0;
        return this.valid;
    }

    protected int validate() {
        return 0;
    }
}
