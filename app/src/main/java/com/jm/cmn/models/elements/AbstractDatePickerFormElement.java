package com.jm.cmn.models.elements;

import android.content.Context;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jakub Muran on 15.02.2017.
 */
public abstract class AbstractDatePickerFormElement extends AbstractComponent implements DatePicker.OnDateChangedListener {

    protected abstract int getDateBindingVariable();


    private OnDateChangedListener mDateChangedListener;
    public void setOnDateChangedListener(OnDateChangedListener listener) {
        mDateChangedListener = listener;
    }

    private Date mDate = new Date();
    public Date getDate() { return mDate; }
    public void setDate(Date date, boolean notify) {
        mDate = date;
        if(mDateChangedListener != null) mDateChangedListener.onDateChanged(mDate);
    }

    public AbstractDatePickerFormElement(int titleResID, Date date) {
        super(titleResID);
        setDate(date, true);
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        setDate(calendar.getTime(), false);
    }

    public interface OnDateChangedListener {
        public void onDateChanged(Date date);
    }
}
