package com.jm.cmn.models;

import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;

/**
 * Created by Jakub Muran on 22.08.2017.
 */
public class Photo implements IAbstractRecyclerViewRendererData {
    private String url;
    public void setUrl(final String url) { this.url = url; }
    public String getUrl() { return this.url; }

    public boolean deletable;

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(!(obj instanceof Photo)) return false;
        final Photo p = (Photo) obj;
        if(this.url != null && p.url != null && url.equalsIgnoreCase(p.url) && this.deletable == p.deletable) return true;
        return super.equals(obj);
    }
}
