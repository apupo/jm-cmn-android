package com.jm.cmn.models.wrappers;

import com.google.gson.annotations.SerializedName;
import com.jm.cmn.models.LoginModel;

/**
 * Created by Jakub Muran on 21.02.2017.
 */
public class LoginModelWrapper {

    @SerializedName("LoginModel")
    public LoginModel loginModel;

    public LoginModelWrapper(LoginModel loginModel) {
        this.loginModel = loginModel;
    }
}
