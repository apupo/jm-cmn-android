package com.jm.cmn.models;

import android.support.v7.widget.LinearLayoutManager;

import com.jm.cmn.adapters.AbstractRecyclerAdapter;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.models.elements.AbstractComponent;

import java.util.List;

/**
 * Created by Jakub Muran on 16.02.2017.
 */

public abstract class AbstractRecyclerViewComponent extends AbstractComponent {

    protected IAbstractView mAbstractView;

    protected int mOrientation = LinearLayoutManager.VERTICAL;
    public int getOrientation() { return this.mOrientation; }
    public void setOrientation(int orientation) {
        this.mOrientation = orientation;
    }

    private AbstractRecyclerAdapter mAdapter;
    public AbstractRecyclerAdapter getAdapter() { return this.mAdapter; }
    public void setAdapter(AbstractRecyclerAdapter adapter) {
        this.mAdapter = adapter;
    }

    public AbstractRecyclerViewComponent(int titleResID, int orientation, AbstractRecyclerAdapter adapter, List<IAbstractRecyclerViewRendererData> source, IAbstractView abstractView) {
        super(titleResID);
        this.mAbstractView = abstractView;
        setOrientation(orientation);
        setAdapter(adapter);
        getAdapter().setSource(source);
    }

    public void add(final IAbstractRecyclerViewRendererData rendererData) {
        getAdapter().getSource().add(rendererData);
        getAdapter().notifyDataSetChanged();
    }

    public void remove(final IAbstractRecyclerViewRendererData rendererData) {
        getAdapter().getSource().remove(rendererData);
        getAdapter().notifyDataSetChanged();
    }
}
