package com.jm.cmn.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.os.Build;

import com.jm.cmn.R;
import com.jm.cmn.enums.AlertType;
import com.jm.cmn.interfaces.ICameraCallback;
import com.jm.cmn.interfaces.ICameraView;
import com.jm.cmn.models.AlertModel;
import com.jm.cmn.utils.RealPathUtil;
import com.jm.cmn.utils.TakeImageUtil;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 24.08.2017.
 * CoverPage s.r.o.
 */

public class AbstractCameraActivity extends AbstractActivity implements ICameraView {

    private ICameraCallback mCameraCallback;
    public ICameraCallback getCameraCallback() { return mCameraCallback; }

    private TakeImageUtil.TakeImageModel mCurrentTakeImage;
    public TakeImageUtil.TakeImageModel getCurrentTakeImage() { return mCurrentTakeImage; }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {

    }

    @Override
    public void obtainPhoto(ICameraCallback callback) {
        if(!TakeImageUtil.isWritePermissionGranted(getContext(), AbstractCameraActivity.this, callback)) return;

        if(TakeImageUtil.isCameraSupported(getContext())) {
            mCameraCallback = callback;
            mCurrentTakeImage = TakeImageUtil.obtainImageFromCameraOrDocuments();
            startActivityForResult(getCurrentTakeImage().requestIntent, TakeImageUtil.TAKE_IMAGE_REQUEST_CODE);
        } else {
            final AlertModel alertModel = new AlertModel();
            alertModel.type = AlertType.OK;
            alertModel.title = getResources().getString(R.string.Global_Alert_Warning_Title);
            alertModel.message = getResources().getString(R.string.Alert_Unsupported_Camera_Body);
            showAlert(alertModel);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == TakeImageUtil.TAKE_IMAGE_REQUEST_CODE) {
                if(getCameraCallback() != null) {
                    if(data != null && data.getData() != null) {
                        getCameraCallback().onCameraResult(getRealPathFromUri(data.getData()));
                    } else {
                        getCameraCallback().onCameraResult(getCurrentTakeImage().imageFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    private String getRealPathFromUri(Uri uri) {
        String realPath = "";

        // SDK < API11
        if (Build.VERSION.SDK_INT < 11)
            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getContext(), uri);
            // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19)
            realPath = RealPathUtil.getRealPathFromURI_API11to18(getContext(), uri);
            // SDK > 19 (Android 4.4)
        else
            realPath = RealPathUtil.getRealPathFromURI_API19(getContext(), uri);

        return realPath;
    }

}
