package com.jm.cmn.views.fragments;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 17.02.2017.
 * CoverPage s.r.o.
 */

public abstract class AbstractDialogFragment extends android.support.v4.app.DialogFragment {

    private View mRoot;
    protected View getRoot() { return mRoot; }

    private ViewDataBinding mViewDataBinding;
    protected ViewDataBinding getBinding() { return mViewDataBinding; }

    private AbstractViewModel mViewModel;
    public AbstractViewModel getAbstractViewModel() {
        return mViewModel;
    }

    public <T> T getViewModel(Class<T> cls) {
        return cls.cast(getAbstractViewModel());
    }

    private IStoreProvider mStoreProvider;
    protected IStoreProvider getStoreProvider() {
        return (IStoreProvider) mRoot.getContext().getApplicationContext();
    }

    private IAbstractView mAbstractView;
    protected IAbstractView getAbstractView() {
        return (IAbstractView) getActivity();
    }

    protected abstract int getLayoutResID();
    protected abstract AbstractViewModel instantiateViewModel();
    protected abstract void onCreateViewCompleted(View root);

    private void bindContentView(LayoutInflater inflater, int layoutResID, ViewGroup container, boolean attachToParent) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, layoutResID, container, attachToParent);
        mRoot = mViewDataBinding.getRoot();
        mViewModel = instantiateViewModel();
        configureDataBindingVariables(mViewModel);
    }

    protected abstract void configureDataBindingVariables(final AbstractViewModel viewModel);

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bindContentView(inflater, getLayoutResID(), container, false);
        onCreateViewCompleted(getRoot());
        return getRoot();
    }

}
