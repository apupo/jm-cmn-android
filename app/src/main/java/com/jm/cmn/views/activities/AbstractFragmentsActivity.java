package com.jm.cmn.views.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.jm.cmn.views.fragments.AbstractFragment;

/**
 * Created by Jakub Muran on 14.02.2017.
 * CoverPage s.r.o.
 */

public abstract class AbstractFragmentsActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected abstract int getContentLayoutID();

    @Override
    public void navigateTo(Class<?> cls, boolean addToBackStack) {
        Fragment fragment = instantiateFragment(cls);
        if(fragment != null) {
            if (addToBackStack) {
                ((AbstractFragment) fragment).setAllowBackPressed(true);
                getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(AbstractFragment.TAG)
                        .replace(getContentLayoutID(), fragment, fragment.getClass().getSimpleName())
                        .commit();
            } else {
                navigateTo(cls);
            }
        } else {
            super.navigateTo(cls, addToBackStack);
        }
    }

    @Override
    public void navigateTo(Class<?> cls, Bundle args, boolean addToBackStack) {
        Fragment fragment = instantiateFragment(cls);
        if(fragment != null) {
            if(addToBackStack) {
                ((AbstractFragment) fragment).setAllowBackPressed(true);
                fragment.setArguments(args);
                getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(AbstractFragment.TAG)
                        .replace(getContentLayoutID(), fragment, fragment.getClass().getSimpleName())
                        .commit();
            } else {
                navigateTo(cls, args);
            }
        } else {
            super.navigateTo(cls, args, addToBackStack);
        }
    }

    @Override
    public void navigateTo(Class<?> cls) {
        Fragment fragment = instantiateFragment(cls);
        if(fragment != null) {
            ((AbstractFragment) fragment).setAllowBackPressed(false);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(getContentLayoutID(), fragment, fragment.getClass().getSimpleName())
                    .commit();
        } else {
            super.navigateTo(cls);
        }
    }

    @Override
    public void navigateTo(Class<?> cls, Bundle args) {
        Fragment fragment = instantiateFragment(cls);
        if(fragment != null) {
            ((AbstractFragment) fragment).setAllowBackPressed(false);
            fragment.setArguments(args);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(getContentLayoutID(), fragment, fragment.getClass().getSimpleName())
                    .commit();
        } else {
            super.navigateTo(cls, args);
        }
    }

    @Override
    public void navigateToBack() {
        navigateToBack(null);
    }

    @Override
    public void navigateToBack(Bundle args) {
        if(getSupportFragmentManager().getBackStackEntryCount() >= 0) {
            AbstractFragment fragment = (AbstractFragment) getSupportFragmentManager().findFragmentById(getContentLayoutID());
            if(fragment == null) {
                super.navigateToBack();
                return;
            }

            if(fragment.isSkipBackPressed()) return;

            if(fragment.isAllowBackPressed()) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.navigateToBack();
            }

        } else {
            super.navigateToBack();
        }
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() >= 0) {
            AbstractFragment fragment = (AbstractFragment) getSupportFragmentManager().findFragmentById(getContentLayoutID());
            if(fragment == null) {
                super.onBackPressed();
                return;
            }

            if(fragment.isSkipBackPressed()) return;

            if(fragment.isAllowBackPressed()) {
                super.onBackPressed();
            } else {
                onBackPressedCanceled();
            }

        } else {
            super.onBackPressed();
        }
    }

    protected void onBackPressedCanceled() {

    }

    private AbstractFragment instantiateFragment(Class<?> cls) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) cls.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (AbstractFragment) fragment;
    }
}
