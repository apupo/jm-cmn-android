package com.jm.cmn.views.fragments;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jm.cmn.R;
import com.jm.cmn.enums.AlertType;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IAlertCallback;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.activities.AbstractActivity;
import com.jm.cmn.views.activities.AbstractNavigationActivity;

/**
 * Created by Jakub Muran on 14.02.2017.
 */
public abstract class AbstractFragment extends Fragment {

    public static final String TAG = AbstractFragment.class.getSimpleName();

    private boolean mAllowBackPressed;
    public boolean isAllowBackPressed() { return mAllowBackPressed; }
    public void setAllowBackPressed(boolean mAllowBackPressed) { this.mAllowBackPressed = mAllowBackPressed; }

    public boolean isSkipBackPressed() { return false; }

    private View mRoot;
    protected View getRoot() { return mRoot; }

    private ViewDataBinding mViewDataBinding;
    protected ViewDataBinding getBinding() { return mViewDataBinding; }

    private AbstractViewModel mViewModel;
    public AbstractViewModel getAbstractViewModel() {
        return mViewModel;
    }

    public <T> T getViewModel(Class<T> cls) {
        return cls.cast(getAbstractViewModel());
    }

    private IStoreProvider mStoreProvider;
    protected IStoreProvider getStoreProvider() {
        return (IStoreProvider) mRoot.getContext().getApplicationContext();
    }

    private IAbstractView mAbstractView;
    protected IAbstractView getAbstractView() {
        return (IAbstractView) getActivity();
    }

    protected abstract int getLayoutResID();
    protected abstract AbstractViewModel instantiateViewModel();
    protected abstract void onCreateViewCompleted(View root);

    private void bindContentView(LayoutInflater inflater, int layoutResID, ViewGroup container, boolean attachToParent) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, layoutResID, container, attachToParent);
        mRoot = mViewDataBinding.getRoot();
        mViewModel = instantiateViewModel();
        configureDataBindingVariables(mViewDataBinding, mViewModel);
    }

    protected abstract void configureDataBindingVariables(final ViewDataBinding viewDataBinding, final AbstractViewModel viewModel);

    @Override
    public void onStop() {
        super.onStop();
        getViewModel(AbstractViewModel.class).onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        getViewModel(AbstractViewModel.class).onStart();
        setActionBarSubtitle(null);
        setHomeNormal();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Called from activity to give a chance to react on back pressed button
     * @return true if the activity should behave default, false otherwise
     */
    public boolean onBackPressed() {
        return true;
    }

    /**
     * Called from activity, when UP navigation is pressed
     * @return true, when the activity should behave default
     */
    public boolean onNavigateUp() { return true; }

    /**
     * Called from activity when popping this fragment
     * from backstack, so that it can hand over some arguments
     * back to previous fragment
     * @return arguments that are handed over to the previous fragment by setArguments()
     */
    public Bundle getArgumentsForBackstackEntry() {
        return null;
    }

    public void setArgumentsFromBackstackEntry(Bundle args) {

    }

    protected void setActionBarTitle(final CharSequence title) {
        final ActionBar actionBar = getActionBar();
        if(actionBar != null) actionBar.setTitle(title);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(getRoot() == null) {
            bindContentView(inflater, getLayoutResID(), container, false);
            onCreateViewCompleted(getRoot());
        }
        return getRoot();
    }

    protected void setActionBarSubtitle(final CharSequence subtitle) {
        final ActionBar actionBar = getActionBar();
        if(actionBar != null) actionBar.setSubtitle(subtitle);
    }

    protected ActionBar getActionBar() {
        if(getActivity() != null && getActivity() instanceof AbstractActivity) {
            return ((AbstractActivity) getActivity()).getSupportActionBar();
        }
        return null;
    }

    protected void setHomeNormal() {
        if(getActivity() instanceof AbstractActivity) {
            ActionBar actionBar = getActionBar();
            if(actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setHomeAsUpIndicator(R.drawable.ic_home);
            }
        }

        setNavigationLocked(false);
    }

    protected void setHomeAsBack() {
        if(getActivity() instanceof AbstractActivity) {
            ActionBar actionBar = getActionBar();
            if(actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeAsUpIndicator(null);
            }
        }

        setNavigationLocked(true);
    }

    protected void setNoHomeNoBack() {
        if(getActivity() instanceof AbstractActivity) {
            ActionBar actionBar = getActionBar();
            if(actionBar != null) {
                actionBar.setHomeAsUpIndicator(null);
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
        }

        setNavigationLocked(true);
    }

    protected void setNavigationLocked(final boolean enabled) {
        if(getActivity() instanceof AbstractNavigationActivity) {
            ((AbstractNavigationActivity) getActivity()).setNavigationLocked(enabled);
        }
    }

}
