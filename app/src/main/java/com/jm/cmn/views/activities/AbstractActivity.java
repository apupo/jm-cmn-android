package com.jm.cmn.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jm.cmn.AbstractApplication;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IApplication;
import com.jm.cmn.interfaces.IServiceProvider;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.managers.AudioRecorder;
import com.jm.cmn.models.AlertModel;
import com.jm.cmn.network.NetworkStateReceiver;
import com.jm.cmn.stores.LocationStore;
import com.jm.cmn.utils.AlertUtil;
import com.jm.cmn.utils.NetworkUtil;
import com.jm.cmn.utils.ScreenUtil;
import com.jm.cmn.utils.TakeFileUtil;
import com.jm.cmn.utils.TakeImageUtil;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.fragments.AbstractDialogFragment;

public abstract class AbstractActivity extends AppCompatActivity implements IAbstractView, NetworkStateReceiver.NetworkStateReceiverListener {

    public static final String PREFS = "PREFS";

    private NetworkStateReceiver mNetworkStateReceiver;

    private MenuItem mSyncMenuItem;

    protected IApplication mApplication;

    protected IStoreProvider mStoreProvider;
    public IStoreProvider getStoreProvider() { return mStoreProvider; }

    protected IServiceProvider mServiceProvider;

    protected AbstractDialogFragment mCurrentDialog;

    private boolean mAllowHardwareButtons = false;
    public boolean wasHardwareButtonsAllowed() { return mAllowHardwareButtons; }
    public void allowHardwareButtons() { mAllowHardwareButtons = true; }

    protected int getToolbarLayoutResId() {
        return getResources().getIdentifier("toolBar", "id", getPackageName());
    }

    protected Toolbar mToolbar;

    private AbstractViewModel mViewModel;
    public AbstractViewModel getAbstractViewModel() {
        return mViewModel;
    }

    public <T> T getViewModel(Class<T> cls) {
        return cls.cast(getAbstractViewModel());
    }

    private ViewDataBinding mViewDataBinding;
    @Override
    public ViewDataBinding getViewDataBinding() {
        return mViewDataBinding;
    }

    @Override
    public void showDialog(AbstractDialogFragment dialog) {
        mCurrentDialog = dialog;
        mCurrentDialog.show(getSupportFragmentManager(), mCurrentDialog.getClass().getSimpleName());
    }

    @Override
    public AbstractDialogFragment getCurrentDialog() {
        return mCurrentDialog;
    }

    @Override
    public void showAlert(AlertModel alert) { AlertUtil.showAlert(alert, this); }

    @Override
    public void navigateTo(Class<?> cls) {
        startActivity(makeIntent(cls));
    }

    @Override
    public void navigateTo(Class<?> cls, Bundle args) {
        startActivity(makeIntent(cls, args));
    }

    @Override
    public void navigateTo(Class<?> cls, boolean addToBackStack) {
        if(addToBackStack) {
            navigateTo(cls);
        } else {
            final Intent intent = makeIntent(cls);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void navigateTo(Class<?> cls, Bundle args, boolean addToBackStack) {
        if(addToBackStack) {
            navigateTo(cls);
        } else {
            final Intent intent = makeIntent(cls, args);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void navigateToBack() {
        onBackPressed();
    }

    @Override
    public void navigateToBack(Bundle args) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return getSharedPreferences(PREFS, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            navigateToBack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void bindContentView(int layoutResId, AbstractViewModel viewModel) {
        mViewModel = viewModel;
        mViewDataBinding = DataBindingUtil.setContentView(this, layoutResId);
        configureDataBindingVariables(mViewDataBinding, viewModel);
        viewModel.configureDataBinding(mViewDataBinding);
        mViewDataBinding.executePendingBindings();

        final View toolbarView = mViewDataBinding.getRoot().findViewById(getToolbarLayoutResId());
        mToolbar = toolbarView != null ? (Toolbar) toolbarView : null;
        if(mToolbar != null) setSupportActionBar(mToolbar);
    }

    protected abstract void configureDataBindingVariables(final ViewDataBinding viewDataBinding, final AbstractViewModel viewModel);

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final boolean out = super.onCreateOptionsMenu(menu);

        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            Drawable icon = item.getIcon();
            if(icon != null) {
                applyTint(icon);
            }
        }

        return out;
    }

    protected void applyTint(Drawable drawable) {
        drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_IN));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkStateReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mApplication.onActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mApplication.onActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerNetworkStateReceiver();

        mApplication = (IApplication) getApplicationContext();
        mApplication.onActivityStart(this);
        mStoreProvider = (IStoreProvider) mApplication;
        mServiceProvider = (IServiceProvider) mApplication;

        ScreenUtil.allowScreenOrientationOnlyOnTablet(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        final AbstractApplication app = (AbstractApplication) getApplicationContext();

        switch (requestCode) {
            case LocationStore.PERMISSION_ACCESS_FINE_LOCATION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    app.getStore(LocationStore.class).requestLocationUpdate();
                }
                break;
            case TakeImageUtil.PERMISSION_WIRTE_EXTERNAL_STORAGE_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TakeImageUtil.writePermissionGrantedResult(true);
                }
                break;
            case TakeFileUtil.PERMISSION_READ_EXTERNAL_STORAGE_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    TakeFileUtil.permissionGrantedResult(true);
                }
                break;
            case AudioRecorder.PERMISIONS_REQUEST_CODE:
                for (int result : grantResults) {
                    if(result != PackageManager.PERMISSION_GRANTED) {
                        AudioRecorder.permissionGrantedResult(false);
                        break;
                    }
                }
                AudioRecorder.permissionGrantedResult(true);
                break;
        }

    }

    @Override
    public void networkAvailable() {
        updateSyncMenuItemVisibility(mSyncMenuItem, true);
    }

    @Override
    public void networkUnavailable() {
        updateSyncMenuItemVisibility(mSyncMenuItem, false);
    }

    protected void manageSyncMenuItemVisibility(MenuItem syncMenuItem) {
        mSyncMenuItem = syncMenuItem;
        updateSyncMenuItemVisibility(mSyncMenuItem, NetworkUtil.isNetworkAvailable(this));
    }

    protected void updateSyncMenuItemVisibility(MenuItem syncMenuItem, boolean networkAvailable) {
        if(syncMenuItem == null) return;
        syncMenuItem.setVisible(networkAvailable);
    }

    private void registerNetworkStateReceiver() {
        if(mNetworkStateReceiver != null) return;
        mNetworkStateReceiver = new NetworkStateReceiver();
        mNetworkStateReceiver.addListener(this);
        registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unregisterNetworkStateReceiver() {
        if(mNetworkStateReceiver == null) return;
        mNetworkStateReceiver.removeListener(this);
        unregisterReceiver(mNetworkStateReceiver);
        mNetworkStateReceiver = null;
    }

    private Intent makeIntent(Class<?> cls) {
        return new Intent(this, cls);
    }

    private Intent makeIntent(Class<?> cls, Bundle args) {
        Intent out = makeIntent(cls);
        out.putExtras(args);
        return out;
    }
}
