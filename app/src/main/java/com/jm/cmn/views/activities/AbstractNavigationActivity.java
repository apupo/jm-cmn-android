package com.jm.cmn.views.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jm.cmn.BR;
import com.jm.cmn.R;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.INavigationView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.NavigationModel;
import com.jm.cmn.receivers.AbstractNavigationBroadcastReceiver;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.viewmodels.NavigationViewModel;
import com.jm.cmn.views.fragments.AbstractFragment;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by Jakub Muran on 4.6.2015.
 */
public abstract class AbstractNavigationActivity extends AbstractCameraActivity implements INavigationView {

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }

    protected DrawerLayout mDrawerLayout;

    protected View mDrawerView;

    public Toolbar getToolbar() { return mToolbar; }

    private int fragmentRewindId = -1;

    protected NavigationViewModel mNavigationViewModel;
    @Override
    public NavigationViewModel getNavigationViewModel() {
        if(mNavigationViewModel == null) mNavigationViewModel = createNavigationViewModel(mStoreProvider, this, createSource());
        return mNavigationViewModel;
    }

    protected NavigationViewModel createNavigationViewModel(final IStoreProvider storeProvider,final INavigationView navigationView, final List<IAbstractRecyclerViewRendererData> source) {
        return new NavigationViewModel(storeProvider, navigationView, source);
    }

    protected int getNavigationLayoutResId() { return R.layout.navigation_layout; }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(navigationBroadcastReceiver);
    }

    private AbstractNavigationBroadcastReceiver navigationBroadcastReceiver = new AbstractNavigationBroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);

            if(intent.hasExtra(NavigationModel.PARCELABLE_KEY)) {
                NavigationModel navigationModel = Parcels.unwrap(intent.getParcelableExtra(NavigationModel.PARCELABLE_KEY));
                onNavigateNotify(navigationModel, intent.getBundleExtra(NavigationModel.ARG_BUNDLE_KEY));
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindContentView(getNavigationLayoutResId(), getNavigationViewModel());

        LocalBroadcastManager.getInstance(this).registerReceiver(navigationBroadcastReceiver, navigationBroadcastReceiver.getIntentFilter());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerView = findViewById(R.id.left_drawer);

        refreshActionBar();

        // try the saved instance values if any
        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey("fragmentRewindId")) {
                this.fragmentRewindId = savedInstanceState.getInt("fragmentRewindId");
            }
        }

        // move to default fragment, but only if we are not comming from savedInstanceState
        // in which case, the framework will restore the fragment for us
        if(savedInstanceState == null && getIntent() != null && getIntent().hasExtra(NavigationModel.BUNDLE_KEY)) {
            Bundle bundle = getIntent().getBundleExtra(NavigationModel.BUNDLE_KEY);
            if(bundle != null && bundle.containsKey(NavigationModel.PARCELABLE_KEY)) {
                NavigationModel model = Parcels.unwrap(bundle.getParcelable(NavigationModel.PARCELABLE_KEY));
                onNavigateNotify(model, null);
            }
        }

        refresh();

        setTitle(R.string.app_name);

        // a hack for coloring edge glow colors
        if (Build.VERSION.SDK_INT < 20) {
            try {
                //TODO: support if no resource exists
                int glowColor = ContextCompat.getColor(this, R.color.support_glow_color);
                //glow
                int glowDrawableId = getResources().getIdentifier("overscroll_glow", "drawable", "android");
                Drawable androidGlow = getResources().getDrawable(glowDrawableId);
                androidGlow.setColorFilter(glowColor, PorterDuff.Mode.SRC_IN);
                //edge
                int edgeDrawableId = getResources().getIdentifier("overscroll_edge", "drawable", "android");
                Drawable androidEdge = getResources().getDrawable(edgeDrawableId);
                androidEdge.setColorFilter(glowColor, PorterDuff.Mode.SRC_IN);
            } catch (Exception e) {
            }
        }

        getNavigationViewModel().onCreate(savedInstanceState);
    }

    @Override
    public void refresh() {
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void setNavigationLocked(boolean locked) {
        if(mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(locked ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(mDrawerView)) {
            mDrawerLayout.closeDrawer(mDrawerView);
            return;
        }

        FragmentManager fm = getSupportFragmentManager();

        // if our fragment reacts to the back pressed
        // and don't wish to use the default behaviour
        AbstractFragment f = getCurrentFragment();
        boolean behaveDefault = false;
        if(f != null) behaveDefault = f.onBackPressed();

        if(behaveDefault == false) return;

        if(fm.getBackStackEntryCount() > 1) {
            handOverBackStackData();
            super.onBackPressed();
        } else {
			/*
			 * this prevents destroyed activity
			 */
            Intent setIntent = new Intent(Intent.ACTION_MAIN);
            setIntent.addCategory(Intent.CATEGORY_HOME);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(setIntent);
        }
    }

    protected void handOverBackStackData() {
        final Bundle args = getCurrentFragment().getArgumentsForBackstackEntry();
        final FragmentManager fm = getSupportFragmentManager();

        if(args != null && fm.getBackStackEntryCount() > 1) {
            FragmentManager.BackStackEntry bse = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 2);
            Fragment pf = fm.findFragmentByTag(bse.getName());
            if(pf != null && pf instanceof AbstractFragment) ((AbstractFragment)pf).setArgumentsFromBackstackEntry(args);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("fragmentRewindId", fragmentRewindId);
        getNavigationViewModel().onSaveInstanceState(outState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("fragmentRewindId", fragmentRewindId);
        getNavigationViewModel().onSaveInstanceState(outState);
    }

    protected boolean necessaryToNavigate() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        AbstractFragment currentFragment = getCurrentFragment();

        if (currentFragment != null && currentFragment.isVisible()) {

            // give the fragment a chance to react to up navigation
            if(!currentFragment.onNavigateUp()) {
                return true;
            };

            // here is a place for some special use cases in the navigation, if needed


            // otherwise we use the back button functionality
            if(fragmentManager.getBackStackEntryCount() > 1) {
                handOverBackStackData();
                super.onBackPressed();
                return true;
            }
        }

        return false;
    }

    @Override
    public void hideNavigationMenu() {
        if(mDrawerLayout == null) return;
        if(mDrawerView == null) return;
        if(mDrawerLayout.isDrawerOpen(mDrawerView)) mDrawerLayout.closeDrawer(mDrawerView);
    }

    @Override
    public void showNavigationMenu() {
        if(mDrawerLayout == null) return;
        if(mDrawerView == null) return;
        if(!mDrawerLayout.isDrawerOpen(mDrawerView)) mDrawerLayout.openDrawer(mDrawerView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if(!necessaryToNavigate()) {
                    if(mDrawerLayout.isDrawerOpen(mDrawerView)) {
                        mDrawerLayout.closeDrawer(mDrawerView);
                    }
                    else {
                        mDrawerLayout.openDrawer(mDrawerView);
                    }
                    return true;
                }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     *    ArrayList<NavigationModel> source = new ArrayList<NavigationModel>();
     *    source.add( makeNavigationModel("Home", getResources().getDrawable(android.R.drawable.ic_btn_speak_now), HomeFragment.class) );
     *    source.add( null ); for horizontal rule
     *    return source;
     */
    public abstract List<IAbstractRecyclerViewRendererData> createSource();

    @Override
    public void onNavigateNotify(final NavigationModel navigate, final Bundle args) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        // special case for back navigation
        if(navigate.isStepBack()) {
            navigate.resetStepBack();
            if(fragmentManager.getBackStackEntryCount() > 1) {
                handOverBackStackData();
                super.onBackPressed();
                return;
            }
        }

        final Fragment fragment = navigate.createFragment();

        // we will rewind the back stack to keep the navigation correct
        final boolean rewind = navigate.isRewind();
        navigate.resetRewind();

        if(mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mDrawerView)) {
            mDrawerLayout.closeDrawers();
            mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {

                }

                @Override
                public void onDrawerOpened(View drawerView) {

                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    if (fragment != null) {
                        fragment.setArguments(args);
                        navigate(fragment, navigate.toString(), rewind);
                    }
                    mDrawerLayout.setDrawerListener(null);
                }

                @Override
                public void onDrawerStateChanged(int newState) {

                }
            });
        } else {
            if (fragment != null) {
                fragment.setArguments(args);
                navigate(fragment, navigate.toString(), rewind);
            }
        }
    }

    protected AbstractFragment getCurrentFragment() {
        return (AbstractFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
    }

    private int navigate(Fragment fragment, String tag, boolean rewind) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        // rewind to clear the back stack
        if(rewind && fragmentRewindId > -1) {
            try {
                fragmentManager.popBackStack(fragmentRewindId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            catch (IllegalStateException e) {
                Log.w("NavigationActivity", "Cannot change fragments after onSaveInstanceState, aborting.");
                return fragmentRewindId;
            }
        }

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out,
                android.R.anim.fade_in,
                android.R.anim.fade_out);

        ft.addToBackStack(tag);
        int fid = ft.replace(R.id.content_frame, fragment, tag).commitAllowingStateLoss();
        // if we rewind, this is our new root
        if(rewind) { fragmentRewindId = fid; }
        return fid;
    }

    protected NavigationModel makeNavigationModel(Object title, int image, Class<? extends AbstractFragment> navigationFragmentClass) {
        return makeNavigationModel(title, image, navigationFragmentClass, false);
    }

    protected NavigationModel makeNavigationModel(Object title, int image, Class<? extends AbstractFragment> navigationFragmentClass, boolean navigateViaBroadcast) {
        String titleString = "";
        if(title != null && title instanceof String) titleString = String.valueOf(title);
        else if(title instanceof Integer) titleString = getResources().getString((int) title);
        return new NavigationModel(titleString, image, navigationFragmentClass, navigateViaBroadcast);
    }

    private void refreshActionBar() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

}
