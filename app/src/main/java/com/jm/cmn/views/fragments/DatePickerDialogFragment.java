package com.jm.cmn.views.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Jakub Muran on 10/26/2017.
 */

public class DatePickerDialogFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener mOnDateSetListener;
    public void setOnDateSetListener(final DatePickerDialog.OnDateSetListener onDateSetListener) {
        this.mOnDateSetListener = onDateSetListener;
    }

    private long mTimeMillis = System.currentTimeMillis();
    public void setTimeMillis(final long timestamp) {
        this.mTimeMillis = timestamp;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(this.mTimeMillis);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog dialog = new DatePickerDialog(getActivity(), this.mOnDateSetListener, year, month, day);
        dialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        return dialog;
    }
}
