package com.jm.cmn.adapters;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractRecyclerViewRendererViewModel;

import java.util.List;

/**
 * Created by Jakub Muran on 16.02.2017.
 */
public abstract class AbstractRecyclerViewViewHolder extends RecyclerView.ViewHolder {

    protected AbstractRecyclerViewRendererViewModel mItemViewModel;

    protected abstract AbstractRecyclerViewRendererViewModel instantiateRendererViewModel(
            IStoreProvider storeProvider,
            IAbstractView view,
            ViewDataBinding viewDataBinding);

    public AbstractRecyclerViewViewHolder(View itemView) {
        super(itemView);
    }

    public AbstractRecyclerViewViewHolder(
            IStoreProvider storeProvider,
            IAbstractView view,
            ViewDataBinding viewDataBinding) {
        super(viewDataBinding.getRoot());
        this.mItemViewModel = instantiateRendererViewModel(storeProvider, view, viewDataBinding);
    }

    public void bind(List<? extends IAbstractRecyclerViewRendererData> source, int position) {
        this.mItemViewModel.bind(itemView, source, position);
    }
}
