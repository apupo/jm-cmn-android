package com.jm.cmn.adapters;

import android.content.Context;
import android.databinding.ViewDataBinding;

import com.jm.cmn.R;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.PhotosComponent;
import com.jm.cmn.viewmodels.AbstractRecyclerViewRendererViewModel;
import com.jm.cmn.viewmodels.PhotoRendererViewModel;

/**
 * Created by Jakub Muran on 16.02.2017.
 */
public class PhotosRecyclerAdapter extends AbstractRecyclerAdapter {

    private PhotosComponent mComponent;
    public void setComponent(PhotosComponent component) {
        this.mComponent = component;
    }

    public PhotosRecyclerAdapter(Context context, IStoreProvider storeProvider) {
        super(context, storeProvider);
    }

    @Override
    protected int getRendererLayoutResID() {
        return R.layout.photo_renderer_layout;
    }

    @Override
    protected AbstractRecyclerViewViewHolder instantiateViewHolder(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
        return new PhotosRecyclerViewViewHolder(storeProvider, view, viewDataBinding);
    }

    public class PhotosRecyclerViewViewHolder extends AbstractRecyclerViewViewHolder {

        public PhotosRecyclerViewViewHolder(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
            super(storeProvider, view, viewDataBinding);
        }

        @Override
        protected AbstractRecyclerViewRendererViewModel instantiateRendererViewModel(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
            return new PhotoRendererViewModel(storeProvider, view, viewDataBinding, PhotosRecyclerAdapter.this.mComponent);
        }
    }

}
