package com.jm.cmn.adapters;

import android.content.Context;
import android.databinding.ViewDataBinding;

import com.jm.cmn.R;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractRecyclerViewRendererViewModel;
import com.jm.cmn.viewmodels.NavigationRendererViewModel;

import java.util.List;

/**
 * Created by Jakub Muran on 9/20/2017.
 */

public class NavigationsRecyclerAdapter extends AbstractRecyclerAdapter {

    public NavigationsRecyclerAdapter(Context context, IStoreProvider storeProvider) {
        super(context, storeProvider);
    }

    @Override
    protected int getRendererLayoutResID() {
        return R.layout.navigation_renderer_layout;
    }

    protected NavigationsRecyclerViewViewHolder createNavigationRecyclerViewViewHolder(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
        return new NavigationsRecyclerViewViewHolder(storeProvider, view, viewDataBinding);
    }

    @Override
    protected AbstractRecyclerViewViewHolder instantiateViewHolder(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
        return createNavigationRecyclerViewViewHolder(storeProvider, view, viewDataBinding);
    }

    public class NavigationsRecyclerViewViewHolder extends AbstractRecyclerViewViewHolder {

        public NavigationsRecyclerViewViewHolder(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
            super(storeProvider, view, viewDataBinding);
        }

        @Override
        protected AbstractRecyclerViewRendererViewModel instantiateRendererViewModel(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
            return createNavigationRendererViewModel(storeProvider, view, viewDataBinding);
        }

        protected NavigationRendererViewModel createNavigationRendererViewModel(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
            return new NavigationRendererViewModel(storeProvider, view, viewDataBinding);
        }

    }
}
