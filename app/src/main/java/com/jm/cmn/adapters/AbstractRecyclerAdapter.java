package com.jm.cmn.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 16.02.2017.
 */
public abstract class AbstractRecyclerAdapter extends RecyclerView.Adapter<AbstractRecyclerViewViewHolder> {

    private List<IAbstractRecyclerViewRendererData> mSource = new ArrayList<>();
    public List<IAbstractRecyclerViewRendererData> getSource() {
        return this.mSource;
    };
    public void setSource(List<IAbstractRecyclerViewRendererData> source) {
        this.mSource = source;
        notifyDataSetChanged();
    }

    private Context mContext;

    private IStoreProvider mStoreProvider;

    private IAbstractView mAbstractView;

    private LayoutInflater mLayoutInflater;

    protected abstract int getRendererLayoutResID();
    protected abstract AbstractRecyclerViewViewHolder instantiateViewHolder(
            IStoreProvider storeProvider,
            IAbstractView view,
            ViewDataBinding viewDataBinding);

    public AbstractRecyclerAdapter(Context context, IStoreProvider storeProvider) {
        this.mContext = context;
        this.mStoreProvider = storeProvider;
        this.mAbstractView = (IAbstractView) context;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public AbstractRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ViewDataBinding viewDataBinding = DataBindingUtil.inflate(
                this.mLayoutInflater,
                getRendererLayoutResID(),
                parent,
                false);
        return instantiateViewHolder(mStoreProvider, mAbstractView, viewDataBinding);
    }

    @Override
    public void onBindViewHolder(AbstractRecyclerViewViewHolder holder, int position) {
        holder.bind(getSource(), position);
    }

    @Override
    public int getItemCount() {
        return this.mSource != null  ? this.mSource.size() : 0;
    }
}
