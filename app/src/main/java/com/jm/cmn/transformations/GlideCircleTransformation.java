package com.jm.cmn.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;

/**
 * Created by Jakub Muran on 04.10.2017.
 */

public class GlideCircleTransformation implements Transformation {

    private BitmapPool mBitmapPool;

    public GlideCircleTransformation(Context context) {
         this(Glide.get(context).getBitmapPool());
    }

    public GlideCircleTransformation(BitmapPool pool) {
        this.mBitmapPool = pool;
    }

    @Override
    public Resource transform(Resource resource, int outWidth, int outHeight) {
        final int dstW = 800;
        final int dstH = 600;

        Bitmap original = (Bitmap) resource.get();

        final int originalW = original.getWidth();
        final int originalH = original.getHeight();

        final double scaleX = (double) dstW / originalW;
        final double scaleY = (double) dstH / originalH;
        final double scale = Math.max(scaleX, scaleY);

        final int scaledW = (int) (originalW * scale);
        final int scaledH = (int) (originalH * scale);

        Bitmap source = Bitmap.createScaledBitmap(original, scaledW, scaledH, false);

        int size = Math.min(source.getWidth(), source.getHeight());

        int width = (source.getWidth() - size) / 2;
        int height = (source.getHeight() - size) / 2;

        Bitmap bitmap = mBitmapPool.get(size, size, Bitmap.Config.ARGB_8888);
        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        Paint strokePaint = new Paint();
        int strokeWidth = 5;
        strokePaint.setColor(0xffffffff);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setStrokeWidth(strokeWidth);

        BitmapShader shader =
                new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        if (width != 0 || height != 0) {
            // source isn't square, move viewport to center
            Matrix matrix = new Matrix();
            matrix.setTranslate(-width, -height);
            shader.setLocalMatrix(matrix);
        }
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);
        canvas.drawCircle(r, r, r - strokeWidth, strokePaint);

        return BitmapResource.obtain(bitmap, mBitmapPool);
    }

    @Override
    public String getId() {
        return "GlideCircleTransformation()";
    }
}
