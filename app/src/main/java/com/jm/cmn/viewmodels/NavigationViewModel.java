package com.jm.cmn.viewmodels;

import android.content.Context;
import android.databinding.Bindable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.jm.cmn.adapters.NavigationsRecyclerAdapter;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.INavigationView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.NavigationModel;

import java.util.List;

/**
 * Created by Jakub Muran on 4.6.2015.
 */
public class NavigationViewModel extends AbstractViewModel {

    private NavigationsRecyclerAdapter mAdapter;
    @Bindable
    public NavigationsRecyclerAdapter getAdapter() { return mAdapter; }

    protected int mOrientation = LinearLayoutManager.VERTICAL;
    @Bindable
    public int getOrientation() { return this.mOrientation; }
    public void setOrientation(int orientation) {
        this.mOrientation = orientation;
    }

    private INavigationView mNavigationView;
    public final INavigationView getNavigationView() { return this.mNavigationView; }

    private List<IAbstractRecyclerViewRendererData> source;
    public void selectedFirst(boolean notify) {
        if(this.source != null && !this.source.isEmpty()) setSelectedItem((NavigationModel) this.source.get(0), notify);
    }
    public void selectedFirst() {
        selectedFirst(false);
    }

    private NavigationModel selectedItem;
    public NavigationModel getSelectedItem() { return selectedItem; }
    public void setSelectedItem(final NavigationModel selectedItem) { setSelectedItem(selectedItem, true); }
    public void setSelectedItem(final NavigationModel selectedItem, final boolean notify) {
        if(isLoading()) return;
/*
        if(selectedItem.getNavigationFragmentClass().equals(RateAppFragment.class) ||
                selectedItem.getNavigationFragmentClass().equals(ShareAppFragment.class)) {
            this.mNavigationView.onNavigateNotify(selectedItem, null);
            return;
        }
*/

        if(this.selectedItem != null && this.selectedItem.getNavigationFragmentClass().equals(selectedItem.getNavigationFragmentClass())) {
            return;
        }

        if(getSource() != null) {
            for (IAbstractRecyclerViewRendererData data : getSource()) {
                final NavigationModel model = data != null && data instanceof NavigationModel ? (NavigationModel) data : null;

                if(model != null && model.getNavigationFragmentClass() != null) {
                    if(!model.getNavigationFragmentClass().equals( selectedItem.getNavigationFragmentClass() )) model.setSelected(false);
                    else {
                        model.setSelected(true);
                        model.setRewind(true);
                        this.selectedItem = model;
                        if(notify) this.mNavigationView.onNavigateNotify(model, null);
                    }
                }
            }

            notifyChange();
        }
    }

    public List<IAbstractRecyclerViewRendererData> getSource() {
        return this.source;
    }

    public NavigationViewModel(IStoreProvider storeProvider, INavigationView navigationView, List<IAbstractRecyclerViewRendererData> source) {
        super(storeProvider, navigationView);
        this.source = source;
        this.mNavigationView = navigationView;
        this.mAdapter = createNavigationsRecyclerAdapter(navigationView.getContext(), storeProvider);
        this.mAdapter.setSource(this.source);
    }

    protected NavigationsRecyclerAdapter createNavigationsRecyclerAdapter(Context context, IStoreProvider storeProvider) {
        return new NavigationsRecyclerAdapter(context, storeProvider);
    }

    public void onCreate(Bundle savedInstanceState) {
        int selectedItemPosition = savedInstanceState != null ? savedInstanceState.getInt("selectedItemPosition", 0) : 0;
        if(getSource() != null) setSelectedItem((NavigationModel) getSource().get(selectedItemPosition), savedInstanceState == null);
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("selectedItemPosition", getSource().indexOf(getSelectedItem()));
    }

}
