package com.jm.cmn.viewmodels;

import android.databinding.Bindable;

import com.jm.cmn.BR;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;

/**
 * Created by Jakub Muran on 31.10.2017.
 */

public class ProgressBarViewModel extends AbstractViewModel {

    private boolean mLoading;
    @Bindable public boolean getLoading() { return this.mLoading; }
    public void setLoading(final boolean loading) {
        this.mLoading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public ProgressBarViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }
}
