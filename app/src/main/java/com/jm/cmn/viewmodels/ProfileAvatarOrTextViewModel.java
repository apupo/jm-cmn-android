package com.jm.cmn.viewmodels;

import android.databinding.Bindable;

import com.jm.cmn.BR;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;

/**
 * Created by Jakub Muran on 12.10.2017.
 */

public class ProfileAvatarOrTextViewModel extends ProfileAvatarViewModel {

    private String mInitials;
    @Bindable public String getInitials() { return this.mInitials; }
    public void setInitials(final String initials) {
        this.mInitials = initials;
        notifyPropertyChanged(BR.initials);
    }

    public ProfileAvatarOrTextViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }
}
