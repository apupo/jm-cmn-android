package com.jm.cmn.viewmodels;

import android.databinding.Bindable;
import android.databinding.ViewDataBinding;
import android.view.View;

import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;

import java.util.List;

/**
 * Created by Jakub Muran on 16.02.2017.
 */
public abstract class AbstractRecyclerViewRendererViewModel extends AbstractViewModel {

    protected ViewDataBinding mViewDataBinding;

    protected IAbstractRecyclerViewRendererData mData;
    protected <T> T getRendererData(Class<T> cls) {
        return cls.cast(mData);
    }

    protected View mRootView;

    @Bindable public boolean isLast() { return this.mPosition == this.mSource.size() - 1; }
    @Bindable public int getPosition() { return this.mPosition; }
    protected int mPosition;

    protected List<? extends IAbstractRecyclerViewRendererData> mSource;

    public AbstractRecyclerViewRendererViewModel(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
        super(storeProvider, view);
        this.mViewDataBinding = viewDataBinding;
    }

    public void bind(View view, List<? extends IAbstractRecyclerViewRendererData> source, int position) {
        this.mRootView = view;
        this.mSource = source;
        this.mData = source.get(position);
        this.mPosition = position;
        configureDataBinding(mViewDataBinding);
        mViewDataBinding.executePendingBindings();
    }

    public abstract void configureDataBinding(ViewDataBinding viewDataBinding);

    public abstract void onRendererClickEvent(View view);
}
