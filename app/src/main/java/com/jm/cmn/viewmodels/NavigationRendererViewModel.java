package com.jm.cmn.viewmodels;

import android.content.Intent;
import android.databinding.Bindable;
import android.databinding.ViewDataBinding;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.jm.cmn.BR;
import com.jm.cmn.R;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.INavigationView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.NavigationModel;
import com.jm.cmn.receivers.AbstractNavigationBroadcastReceiver;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by Jakub Muran on 4.6.2015.
 */

public class NavigationRendererViewModel extends AbstractRecyclerViewRendererViewModel {

    protected final INavigationView getNavigationView() {
        return getView() != null && getView() instanceof INavigationView ? (INavigationView) getView() : null;
    }

    @Bindable public String getTitle() { return getModel() != null ? getModel().getTitle() : null; }

    @Bindable public int getTitleVisibility() { return (getModel() != null && getModel().getTitle() != null && !getModel().getTitle().isEmpty()) ? View.VISIBLE : View.GONE; }

    @Bindable public int getIconVisibility() { return (getModel() != null && getModel().getIcon() != 0) ? View.VISIBLE : View.GONE; }

    @Bindable public int getIcon() { return getModel() != null ? getModel().getIcon() : 0; }

    @Bindable public int getTextColor() { return isSelected() ? R.color.navigation_item_selected_text_color : R.color.navigation_item_unselected_text_color; }

    @Bindable public int getBg() { return isSelected() ? R.color.navigation_item_selected_bg_color : R.color.navigation_item_unselected_bg_color; }

    @Bindable public int getBadgeBg() { return isSelected() ? R.drawable.badge_circle_background_selected : R.drawable.badge_circle_background_unselected; }

    @Bindable public int getBadgeVisibility() { return getBadgeCount() > 0 ? View.VISIBLE : View.GONE; }

    @Bindable public int getSeparatorVisibility() { return getModel() == null ? View.VISIBLE : View.INVISIBLE; }

    @Bindable public CharSequence getBadgeCountText() { return String.valueOf(getBadgeCount()); }
    public int getBadgeCount() { return getModel() != null ? getModel().badgeCount : 0; }
    public void setBadgeCount(int count) {
        notifyPropertyChanged(BR.badgeCountText);
        notifyPropertyChanged(BR.badgeVisibility);
    }

    public boolean isSelected() {
        return this.mModel != null && this.mModel.isSelected();
    }

    private NavigationModel mModel;
    public NavigationModel getModel() {
        return mModel;
    }
    public void setModel(NavigationModel model) {
        this.mModel = model;
        notifyChange();
    }

    public NavigationRendererViewModel(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding viewDataBinding) {
        super(storeProvider, view, viewDataBinding);
    }

    @Override
    public void bind(View view, List<? extends IAbstractRecyclerViewRendererData> source, int position) {
        super.bind(view, source, position);
        setModel((NavigationModel) source.get(position));
    }

    @Override
    public void configureDataBinding(ViewDataBinding viewDataBinding) {
        viewDataBinding.setVariable(BR.renderer, this);
    }

    @Override
    public void onRendererClickEvent(View view) {
        if(getModel().isNavigateViaBroadcast()) {
            final Intent intent = AbstractNavigationBroadcastReceiver.createIntent();
            intent.putExtra(NavigationModel.PARCELABLE_KEY, Parcels.wrap(getModel()));
            LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(intent);
        } else {
            getNavigationView().getNavigationViewModel().setSelectedItem(getModel());
        }

    }

}
