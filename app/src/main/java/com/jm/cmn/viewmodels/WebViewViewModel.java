package com.jm.cmn.viewmodels;

import android.databinding.Bindable;

import com.jm.cmn.BR;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;

/**
 * Created by Jakub Muran on 10/5/2017.
 */

public class WebViewViewModel extends ProgressBarViewModel {

    private String mUrl;
    @Bindable public String getUrl() { return this.mUrl; }
    protected void setUrl(final String url) {
        this.mUrl = url;
        notifyPropertyChanged(BR.url);
    }

    private String mAuthName;
    @Bindable public String getAuthName() { return this.mAuthName; }
    protected void setAuthName(final String authName) {
        this.mAuthName = authName;
        notifyPropertyChanged(BR.authName);
    }

    private String mAuthPass;
    @Bindable public String getAuthPass() { return this.mAuthPass; }
    protected void setAuthPass(final String authPass) {
        this.mAuthPass = authPass;
        notifyPropertyChanged(BR.authPass);
    }


    public WebViewViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }

    public void load(final String url) {
        setUrl(url);
    }

}
