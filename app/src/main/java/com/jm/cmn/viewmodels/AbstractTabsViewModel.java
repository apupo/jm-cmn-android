package com.jm.cmn.viewmodels;

import android.databinding.Bindable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.jm.cmn.BR;
import com.jm.cmn.R;
import com.jm.cmn.factories.TabFactory;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.TabInfoModel;

/**
 * Created by Jakub Muran on 22.09.2017.
 */
abstract public class AbstractTabsViewModel extends AbstractViewModel implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

    private TabHost mTabHost;
    public void setTabHost(final TabHost tabHost) { this.mTabHost = tabHost; }
    public TabHost getTabHost() { return mTabHost; }

    private Bundle mSaveInstaceState;
    public Bundle getSaveInstanceState() { return this.mSaveInstaceState; }

    private int mCurrentPage;
    @Bindable
    public int getCurrentPage() { return this.mCurrentPage; }
    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
        notifyPropertyChanged(BR.currentPage);
    }

    abstract public void addTabs(final Bundle args);

    abstract protected PagerAdapter createAdapter();

    private PagerAdapter mPagerAdapter;
    @Bindable
    public PagerAdapter getPagerAdapter() {
        if(this.mPagerAdapter == null) this.mPagerAdapter = createAdapter();
        return this.mPagerAdapter;
    }

    protected void addTab(final String tag, final String indicator, final Class<?> cls, final Bundle args) {
        TabInfoModel tabInfo = new TabInfoModel(tag, cls, args);
        addTabInfo(getTabHost(), getTabHost().newTabSpec(tag).setIndicator(createTabView(getTabHost(), indicator)), tabInfo);
    }

    private void addTabInfo(TabHost tabHost, TabHost.TabSpec tabSpec, TabInfoModel tabInfo) {
        tabSpec.setContent(new TabFactory(tabHost.getContext()));
        tabHost.addTab(tabSpec);
    }

    protected View createTabView(final TabHost tabHost, final String text) {
        View view = LayoutInflater.from(tabHost.getContext()).inflate(R.layout.tab_layout, null);
        TextView tv = (TextView) view.findViewById(R.id.tabTextView);
        tv.setText(text);
        return view;
    }

    public AbstractTabsViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
        setCurrentPage(0);
    }

    @Override
    public void onTabChanged(String tabId) {
        setCurrentPage(mTabHost.getCurrentTab());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
