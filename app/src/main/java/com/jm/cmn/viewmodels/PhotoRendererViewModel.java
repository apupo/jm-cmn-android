package com.jm.cmn.viewmodels;

import android.databinding.ViewDataBinding;
import android.view.View;
import android.widget.ImageView;

import com.jm.cmn.BR;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.Photo;
import com.jm.cmn.models.PhotosComponent;

/**
 * Created by Jakub Muran on 03.06.2016.
 */
public class PhotoRendererViewModel extends AbstractRecyclerViewRendererViewModel {

    private PhotosComponent mComponent;

    public ImageView.ScaleType getScaleType() {
        return ImageView.ScaleType.CENTER_CROP;
    }

    public Photo getPhoto() {
        return getRendererData(Photo.class);
    }

    public PhotoRendererViewModel(IStoreProvider storeProvider, IAbstractView view, ViewDataBinding binding, PhotosComponent component) {
        super(storeProvider, view, binding);
        this.mComponent = component;
    }

    @Override
    public void configureDataBinding(ViewDataBinding viewDataBinding) {
        viewDataBinding.setVariable(BR.renderer, this);
    }

    @Override
    public void onRendererClickEvent(View view) {}

    public void onDeleteClickEvent(View itemView) {
        IAbstractRecyclerViewRendererData toDelete = null;
        for (IAbstractRecyclerViewRendererData model : this.mComponent.getAdapter().getSource()) {
            if(model instanceof Photo) {
                Photo photo = (Photo) model;
                if(photo.equals(getPhoto())) {
                    toDelete = photo;
                    break;
                }
            }
        }

        this.mComponent.remove(toDelete);
        this.mComponent.dispatchOnPhotosChanged(null, (Photo) toDelete);
    }
}
