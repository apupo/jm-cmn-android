package com.jm.cmn.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ViewDataBinding;

import com.jm.cmn.BR;
import com.jm.cmn.enums.AlertType;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IAlertCallback;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.AlertModel;

/**
 * Created by Jakub Muran on 23.7.2015.
 */

public abstract class AbstractViewModel extends BaseObservable {

    private IAbstractView mView;
    public IAbstractView getView() { return this.mView; }

    private IStoreProvider mStoreProvider;
    public IStoreProvider getStoreProvider() { return this.mStoreProvider; }

    protected boolean mLoading;
    @Bindable public boolean isLoading() {
        return this.mLoading;
    }
    public void setLoading(boolean loading) {
        if(this.mLoading == loading) return;
        this.mLoading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public AbstractViewModel(IStoreProvider storeProvider, IAbstractView view) {
        this.mStoreProvider = storeProvider;
        this.mView = view;
    }

    public void configureDataBinding(final ViewDataBinding viewDataBinding) {}

    /**
     * Called upon removal, view models should make a clean up.
     * They cannot be used after this call.
     */
    public void onRemove() {

    }

    public void onStop() {

    }

    public void onStart() {

    }

    protected int generateHasCode(Object... args) {
        int hasCode = 0;
        for(int i = 0; i < args.length; i++) {
            if(args[i] != null) hasCode += args[i].hashCode();
        }
        return hasCode;
    };

    protected AlertModel makeErrorAlertModel(final Object title, final Object message, final IAlertCallback alertCallback) {
        final AlertModel out = new AlertModel();
        out.callback = alertCallback;
        out.type = AlertType.OK;

        if(title instanceof String) {
            out.title = String.valueOf(title);
        } else if(title instanceof Integer){
            out.title = mView.getResources().getString((int)title);
        }

        if(message instanceof String) {
            out.message = String.valueOf(message);
        } else if(message instanceof Integer) {
            out.message = mView.getResources().getString((int)message);
        }

        return out;
    }

    protected AlertModel makeErrorAlertModel(final Object title, final Object message) {
        return makeErrorAlertModel(title, message, null);
    }

}
