package com.jm.cmn.viewmodels;

import android.databinding.Bindable;
import android.text.TextUtils;
import android.view.View;

import com.jm.cmn.BR;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.ICameraCallback;
import com.jm.cmn.interfaces.ICameraView;
import com.jm.cmn.interfaces.IStoreProvider;

/**
 * Created by Jakub Muran on 06.10.2017.
 */

public class ProfileAvatarViewModel extends AbstractViewModel implements ICameraCallback {

    public interface OnImageChangeListener {
        void onChanged(final String imagePath);
    }

    private OnImageChangeListener mImageChangeListener;
    public void setOnImageChangeListener(final OnImageChangeListener listener) {
        this.mImageChangeListener = listener;
    }

    private String mAvatarImagePath;
    @Bindable
    public String getAvatarImagePath() { return this.mAvatarImagePath; }
    public void setAvatarImagePath(final String path) {
        if(!TextUtils.isEmpty(this.mAvatarImagePath) && this.mAvatarImagePath.equalsIgnoreCase(path)) return;
        this.mAvatarImagePath = path;
        if(this.mImageChangeListener != null) this.mImageChangeListener.onChanged(this.mAvatarImagePath);
        notifyPropertyChanged(BR.avatarImagePath);
        notifyPropertyChanged(BR.initials);
    }

    private String mInitials;
    @Bindable
    public String getInitials() { return hasImagePath() ? null : this.mInitials; } //no initials if has image path
    public void setInitials(final String initials) {
        this.mInitials = initials;
        notifyPropertyChanged(BR.initials);
    }

    public boolean hasImagePath() {
        return !TextUtils.isEmpty(getAvatarImagePath());
    }

    public ProfileAvatarViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }

    public void addAvatarClick(View view) {
        if(isLoading()) return;
        ((ICameraView) getView()).obtainPhoto(this);
    }

    @Override
    public void onCameraResult(Object object) {
        setAvatarImagePath(String.valueOf(object));
    }

}
