package com.jm.cmn.viewmodels;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.jm.cmn.R;
import com.jm.cmn.databinding.PassCodeComponentLayoutBinding;
import com.jm.cmn.models.elements.AbstractComponent;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author MURAN 12/12/2013
 */

public class PassCodeComponent extends AbstractComponent {

	private ViewDataBinding mViewDataBinding;

	private Context mContext;
	public Context getContext() { return this.mContext; }

	public PassCodeComponent(Context context, int titleResID) {
		super(titleResID);
		this.mContext = context;
		init();
	}

	protected List<EditText> mInputs;
	
	private OnCodeEnteredListener mOnCodeEnteredListener;
	public void setOnCodeEnteredListener(OnCodeEnteredListener codeEnteredListener) { this.mOnCodeEnteredListener = codeEnteredListener; }
	
	protected Handler mHandler;

	private boolean mDispatchAfterFill;
	public void setDispatchAfterFill(boolean dispatchAfterFill) {
		this.mDispatchAfterFill = dispatchAfterFill;
	}

	protected TextView mTitleTextView;
	public void setTitle(final Object title) {
		if(title == null) return;
		if(title instanceof String) this.mTitleTextView.setText(String.valueOf(title));
		if(title instanceof Integer) this.mTitleTextView.setText((int) title);
	}
	
	private boolean mShowKeyboard = true;
	public void setShowKeyboard(boolean show) { 
		mShowKeyboard = show;
	}

	public void clear() {
		for (EditText editText : mInputs) {
			editText.clearFocus();
			editText.getText().clear();
		}
	}
	
	public void show() {
		setVisible(true);
		showKeyboard();
	}

	protected void showKeyboard() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				mInputs.get(0).setFocusableInTouchMode(true);
				mInputs.get(0).requestFocus();

				if(mShowKeyboard) {
					mInputs.get(0).dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN , 0, 0, 0));
					mInputs.get(0).dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0));
				} else {
					mShowKeyboard = true;
				}

				mInputs.get(0).setFocusableInTouchMode(false);
			}
		}, 500);
	}
	
	public void hide() {
		InputMethodManager imm = (InputMethodManager)  getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(this.mViewDataBinding.getRoot().getWindowToken(), 0);
		
		setVisible(false);

		this.mViewDataBinding.getRoot().clearFocus();
		
		clear();
	}
	
	public void setCode(String code) {
		if(code == null || code.length() != mInputs.size()) {
			clear();
			return;
		}
		
		for(int i = 0; i < code.length(); i++) {
			this.mInputs.get(i).setText(code.substring(i, i+1));
		}
		
		mHandler.postDelayed(mRunnable, 1000);
	}
	
	protected String getCode() {
		String code = "";
		
		for (EditText editText : mInputs) {
			code += editText.getText();
		}
		
		return code;
	}
	
	protected void init() {
		final LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mViewDataBinding = DataBindingUtil.inflate(layoutInflater, R.layout.pass_code_component_layout, null, true);
		this.mViewDataBinding.executePendingBindings();

		this.mHandler = new Handler();

		this.mInputs = new ArrayList<EditText>();
		
		EditText editText = null;

		List<Integer> ids = new ArrayList<Integer>();
		ids.add(R.id.pass_code_edit1);
		ids.add(R.id.pass_code_edit2);
		ids.add(R.id.pass_code_edit3);
		ids.add(R.id.pass_code_edit4);

		for (Integer id : ids) {
			editText = (EditText) this.mViewDataBinding.getRoot().findViewById(id);
			editText.setOnEditorActionListener(this.onEditorActionListener);
			editText.addTextChangedListener(this.onTextChangeListener);
			this.mInputs.add(editText);
		}

		show();
	}
	
	private Runnable mRunnable = new Runnable() {
		
		@Override
		public void run() {
			dispatchCode();
			mHandler.removeCallbacks(mRunnable);
		}
	};
	
	private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
		
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if(actionId == EditorInfo.IME_ACTION_DONE) {
				dispatchCode();
			}
			return true;
		}
	};

	private void dispatchCode() {
		if(this.mOnCodeEnteredListener != null) this.mOnCodeEnteredListener.onCodeEntered(PassCodeComponent.this, getCode());
	}

	private TextWatcher onTextChangeListener = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			int index;
			
			EditText next = null;
			
			for (EditText editText : mInputs) {
				
				if( editText.hasFocus() ) {
					
					index = mInputs.indexOf(editText);
					
					 if(editText.getText().length() > 0) {
						 index++;
					 } else {
						 index--;
					 }
					
					try {
						next = mInputs.get( index );
					} catch (Exception e) {
						editText.requestFocus();
						if(mDispatchAfterFill) {
							PassCodeComponent.this.mHandler.postDelayed(new Runnable() {
								@Override
								public void run() {
									dispatchCode();
								}
							}, 400);
						}
						break;
					}

					if( next != null ) {
						next.setFocusableInTouchMode(true);
						next.requestFocus();
						next.setFocusableInTouchMode(false);
						break;
					}
					
				} 				
			}
		}
	};
	
	public interface OnCodeEnteredListener {
		public void onCodeEntered(PassCodeComponent view, String code);
	}

}
