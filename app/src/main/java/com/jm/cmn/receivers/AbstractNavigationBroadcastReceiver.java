package com.jm.cmn.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by Jakub Muran on 1/28/2016.
 */
public class AbstractNavigationBroadcastReceiver extends BroadcastReceiver {

    public static final String NAVIGATION_EVENT = "navigate_notify_event";

    private IntentFilter intentFilter;
    public IntentFilter getIntentFilter() {
        if(intentFilter == null) {
            intentFilter = populateIntentFilter();
        }
        return intentFilter;
    }

    public static Intent createIntent() {
        return new Intent(NAVIGATION_EVENT);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

    }

    protected IntentFilter populateIntentFilter() {
        return new IntentFilter(NAVIGATION_EVENT);
    }

}
