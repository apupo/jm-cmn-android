package com.jm.cmn.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.jm.cmn.utils.FileUtil;

/**
 * Created by Jakub Muran on 11/1/2017.
 */

public class SaveBitmapToInternalStorageTask extends AsyncTask<String, Void, String> {

    private Context mContext;

    private int mBitmapW;
    private int mBitmapH;

    private Bitmap.CompressFormat mCompressFormat;

    private String mFilename;

    private OnFileSaveListener mListener;

    public interface OnFileSaveListener {
        void onSaved(String path);
    }

    @Override
    protected String doInBackground(String... params) {
        final String url = params[0];

        final Bitmap original = BitmapFactory.decodeFile(url);

        final int dstW = this.mBitmapW;
        final int dstH = this.mBitmapH;

        final int originalW = original.getWidth();
        final int originalH = original.getHeight();

        final double scaleX = (double) dstW / originalW;
        final double scaleY = (double) dstH / originalH;
        final double scale = Math.max(scaleX, scaleY);

        final int scaledW = (int) (originalW * scale);
        final int scaledH = (int) (originalH * scale);

        Bitmap source = Bitmap.createScaledBitmap(original, scaledW, scaledH, false);

        String path = FileUtil.saveBitmapToInternalStorage(this.mContext, source, this.mFilename, this.mCompressFormat);

        return path;
    }

    @Override
    protected void onPostExecute(String path) {
        super.onPostExecute(path);
        this.mListener.onSaved(path);
    }

    public SaveBitmapToInternalStorageTask(final Context context, String fileName, final int bitmapW, final int bitmapH, Bitmap.CompressFormat compressFormat, final OnFileSaveListener listener) {
        this.mContext = context;

        this.mFilename = fileName;

        this.mBitmapW = bitmapW;
        this.mBitmapH = bitmapH;

        this.mCompressFormat = compressFormat;

        this.mListener = listener;
    }
}
