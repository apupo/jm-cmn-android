package com.jm.cmn.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import com.jm.cmn.models.DownloadModel;
import com.jm.cmn.utils.FileUtil;

/**
 * Created by Jakub Muran on 09.11.2017.
 */
public class DownloadFileTask extends AsyncTask<DownloadModel, Void, DownloadModel> {

    private Context mContext;

    private OnDownloadFileFinishListener mFinishListener;

    public interface OnDownloadFileFinishListener {
        void onFinished(DownloadModel downloadModel);
        void onError(Exception error);
    }

    @Override
    protected DownloadModel doInBackground(DownloadModel... params) {
        try {
            FileUtil.downloadFileInternally(mContext, params[0]);
        } catch (Exception error) {
            error.printStackTrace();
        }

        return params[0];
    }

    @Override
    protected void onPostExecute(DownloadModel downloadModel) {
        if(downloadModel != null) {
            mFinishListener.onFinished(downloadModel);
        } else {
            mFinishListener.onError(new Exception("Cannot download file."));
        }
    }

    public DownloadFileTask(Context context, OnDownloadFileFinishListener finishListener) {
        mContext = context;
        mFinishListener = finishListener;
    }
}
