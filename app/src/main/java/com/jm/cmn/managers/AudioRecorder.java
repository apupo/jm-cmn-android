package com.jm.cmn.managers;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.jm.cmn.views.activities.AbstractActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 19.09.2017.
 */
public class AudioRecorder {

    private static final String LOG_TAG = "AudioRecorder";

    public static final int PERMISIONS_REQUEST_CODE = 502;

    public enum State {
        IDLE, RECORDING, ERROR;
    }

    public interface OnAudioRecorderPermissionsGrantedListener {
        public void onGranted(final boolean granted);
    }

    public interface OnStateChangedListener {
        public void onStateChanged(final State oldState, final State newState);
    }

    public interface OnRecordingTimeElapsedListener {
        public void onRecordingTimeElapsed(final long recordingTimeElapsed);
    }

    private static OnAudioRecorderPermissionsGrantedListener mOnAudioRecorderPermissionsGrantedListener;

    private List<OnStateChangedListener> mOnStateChangedListeners;
    private List<OnRecordingTimeElapsedListener> mOnRecordingTimeElapsedListeners;

    public void registerOnStateChangedListener(final OnStateChangedListener listener) {
        if(this.mOnStateChangedListeners.contains(listener)) return;
        this.mOnStateChangedListeners.add(listener);
    }

    public void unregisterOnStateChangedListener(final OnStateChangedListener listener) {
        if(!this.mOnStateChangedListeners.contains(listener)) return;
        this.mOnStateChangedListeners.remove(listener);
    }

    public void registerOnRecordingTimeElapsedListener(final OnRecordingTimeElapsedListener listener) {
        if(this.mOnRecordingTimeElapsedListeners.contains(listener)) return;
        this.mOnRecordingTimeElapsedListeners.add(listener);
    }

    public void unregisterOnRecordingTimeElapsedListener(final OnRecordingTimeElapsedListener listener) {
        if(!this.mOnRecordingTimeElapsedListeners.contains(listener)) return;
        this.mOnRecordingTimeElapsedListeners.remove(listener);
    }

    private State mState = State.IDLE;
    public State getState() { return this.mState; }
    private void setState(final State newState) {
        if(this.mState == newState) return;
        final State oldState = this.mState;
        this.mState = newState;
        dispatchStateChanged(oldState, this.mState);
    }

    private MediaRecorder mRecorder;

    private int mAudioSource = MediaRecorder.AudioSource.MIC;
    private int mOutputFormat = MediaRecorder.OutputFormat.THREE_GPP;
    private int mDecoder = MediaRecorder.AudioEncoder.HE_AAC;
    private int mRecordingTime = 30 * 1000;

    private long mRecordingTimeElapsed = 0l;
    public long getRecordingTimeElapsed() { return this.mRecordingTimeElapsed; }
    protected void setRecordingTimeElapsed(final long timeElapsed) {
        this.mRecordingTimeElapsed = timeElapsed;
        dispatchRecordingTimeElapsed(this.mRecordingTimeElapsed);
    }

    private CountDownTimer mCountDownTimer;

    private String mFilePath = null;

    public AudioRecorder(final String filePath) {
        this.mOnStateChangedListeners = new ArrayList<>();
        this.mOnRecordingTimeElapsedListeners = new ArrayList<>();
        this.mFilePath = filePath;
    }

    public AudioRecorder(final String filePath, final int audioSource) {
        this(filePath);
        this.mAudioSource = audioSource;
    }

    public AudioRecorder(final String filePath, final int audioSource, final int outputFormat) {
        this(filePath ,audioSource);
        this.mOutputFormat = outputFormat;
    }

    public AudioRecorder(final String filePath, final int audioSource, final int outputFormat, final int audioDecoder) {
        this(filePath ,audioSource, outputFormat);
        this.mDecoder = audioDecoder;
    }

    public AudioRecorder(final String filePath, final int audioSource, final int outputFormat, final int audioDecoder, final int recordingTime) {
        this(filePath ,audioSource, outputFormat, audioDecoder);
        this.mRecordingTime = recordingTime;
    }

    public void startRecording() {
        if(getState() == State.ERROR) return;
        if(getState() == State.RECORDING) return;

        this.mRecorder = new MediaRecorder();
        this.mRecorder.setAudioSource(this.mAudioSource);
        this.mRecorder.setOutputFormat(this.mOutputFormat);
        this.mRecorder.setOutputFile(this.mFilePath);
        this.mRecorder.setAudioEncoder(this.mDecoder);

        try {
            this.mRecorder.prepare();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Preparing failed.");
            setState(State.ERROR);
        }

        if(getState() != State.ERROR) {
            this.mRecorder.start();

            this.mCountDownTimer = new CountDownTimer(this.mRecordingTime, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    setRecordingTimeElapsed(millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    stopRecording();
                }
            };
            this.mCountDownTimer.start();

            setState(State.RECORDING);
        }
    }

    public static void checkRecordAudioPermissionGranted(final AbstractActivity activity, OnAudioRecorderPermissionsGrantedListener listener) {
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) { //no granted
            mOnAudioRecorderPermissionsGrantedListener = listener;
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISIONS_REQUEST_CODE);
        } else {
            listener.onGranted(true);
        }
    }

    public static void permissionGrantedResult(final boolean granted) {
        if(mOnAudioRecorderPermissionsGrantedListener != null) {
            mOnAudioRecorderPermissionsGrantedListener.onGranted(granted);
        }
        mOnAudioRecorderPermissionsGrantedListener = null;
    }

    public void stopRecording() {
        if(getState() == State.ERROR) return;
        if(getState() == State.IDLE) return;

        this.mRecorder.stop();
        this.mRecorder.release();
        this.mRecorder = null;

        this.mCountDownTimer.cancel();
        this.mCountDownTimer = null;

        setRecordingTimeElapsed(0l);

        setState(State.IDLE);
    }

    private void dispatchStateChanged(final State oldState, final State newState) {
        for (final OnStateChangedListener listener : this.mOnStateChangedListeners) {
            listener.onStateChanged(oldState, newState);
        }
    }

    private void dispatchRecordingTimeElapsed(final long elapsedTime) {
        for (final OnRecordingTimeElapsedListener listener : this.mOnRecordingTimeElapsedListeners) {
            listener.onRecordingTimeElapsed(elapsedTime);
        }
    }

}
