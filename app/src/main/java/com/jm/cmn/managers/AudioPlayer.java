package com.jm.cmn.managers;

import android.media.MediaPlayer;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 20.09.2017.
 * CoverPage s.r.o.
 */
public class AudioPlayer implements MediaPlayer.OnCompletionListener {

    private static final String LOG_TAG = "AudioPlayer";

    public enum State {
        IDLE, PLAYING, ERROR;
    }

    public interface OnStateChangedListener {
        public void onStateChanged(final State oldState, final State newState);
    }

    private List<OnStateChangedListener> mOnStateChangedListeners = new ArrayList<>();

    public void registerOnStateChangedListener(final OnStateChangedListener listener) {
        if(this.mOnStateChangedListeners.contains(listener)) return;
        this.mOnStateChangedListeners.add(listener);
    }

    public void unregisterOnStateChangedListener(final OnStateChangedListener listener) {
        if(!this.mOnStateChangedListeners.contains(listener)) return;
        this.mOnStateChangedListeners.remove(listener);
    }

    private State mState = State.IDLE;
    public State getState() { return this.mState; }
    protected void setState(final State newState) {
        if(this.mState == newState) return;
        final State oldState = this.mState;
        this.mState = newState;
        dispatchStateChanged(oldState, newState);
    }

    private MediaPlayer mMediaPlayer;

    private String mFilePath;

    public AudioPlayer(String filePath) {
        this.mFilePath = filePath;
    }

    public void startPlaying() {
        if(getState() == State.PLAYING) return;

        this.mMediaPlayer = new MediaPlayer();
        try {
            this.mMediaPlayer.setDataSource(this.mFilePath);
            this.mMediaPlayer.setOnCompletionListener(this);
            this.mMediaPlayer.prepare();
            this.mMediaPlayer.start();
            setState(State.PLAYING);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Preparing failed.");
            setState(State.ERROR);
        }
    }

    public void stopPlaying() {
        if(getState() == State.IDLE) return;
        if(this.mMediaPlayer == null) return;

        this.mMediaPlayer.release();
        this.mMediaPlayer = null;
        setState(State.IDLE);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopPlaying();
    }

    private void dispatchStateChanged(final State oldState, final State newState) {
        for (final OnStateChangedListener listener : this.mOnStateChangedListeners) {
            listener.onStateChanged(oldState, newState);
        }
    }

}
