package com.jm.cmn.managers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

import com.jm.cmn.AbstractApplication;
import com.jm.cmn.interfaces.IStoreProvider;

/**
 * Created by Jakub Muran on 20.02.2017.
 */
public class SyncManager {

    private static final String TAG                                         =   "sync";

    private static final int SYNC_IMPULSE                                   =   15 * 1000;

    public static final int STOPPING_WHAT                                   =   101;

    public static final String TYPE                                         =   "sync";
    public static final String BUNDLE_KEY                                   =   "bundle_key";
    public static final String ACTION_KEY                                   =   "action_key";
    public static final String ACTION_FINISHED                              =   "sync_finished";
    public static final String ACTION_STOPPED                               =   "sync_stopped";

    private IStoreProvider mStoreProvider;
    public IStoreProvider getStoreProvider() { return this.mStoreProvider; }

    private Handler mHandler;

    private OnSyncFinishedCallback mCallback;
    public void setCallback(OnSyncFinishedCallback callback) {
        mCallback = callback;
    }
    protected OnSyncFinishedCallback getSyncFinishedCallback() { return this.mCallback; }

    public void clearCallback() {
        mCallback = null;
    }

    private State mState = State.IDLE;
    protected State getState() { return mState; }
    protected void setState(State state) {
        if(mState == state) return;
        mState = state;
    }

    public enum State {
        IDLE, SYNCING, STOPPING;
    }

    public static IntentFilter getIntentFilter() {
        return new IntentFilter(TYPE);
    }

    public static Intent getBroadcastIntent() {
        return new Intent(TYPE);
    }

    public SyncManager(IStoreProvider storeProvider) {
        mStoreProvider = storeProvider;
        mHandler = new Handler(mHandlerCallback);
    }

    public void performSync(OnSyncFinishedCallback callback) {
        mCallback = callback;
        performSync();
    }

    public void performSync() {
        if(getState() != State.IDLE) return;
        setState(State.SYNCING);
    }

    public void stopSync() {
        if(getState() != State.SYNCING) return;
        removeMessages();
        sendMessage(STOPPING_WHAT);
    }

    protected void dispatchBroadcast(final String action) {
        final Context ctx = (AbstractApplication) getStoreProvider();
        final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(ctx);
        final Intent intent = getBroadcastIntent();
        final Bundle bundle = new Bundle();
        bundle.putString(ACTION_KEY, action);
        intent.putExtra(BUNDLE_KEY, bundle);
        lbm.sendBroadcast(intent);
    }

    private void removeMessages() {
        mHandler.removeMessages(STOPPING_WHAT);
    }

    private Handler.Callback mHandlerCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if(msg == null) return false;

            final int what = msg.what;

            switch (what) {
                case STOPPING_WHAT:
                    removeMessages();
                    setState(State.IDLE);
                    dispatchBroadcast(ACTION_STOPPED);
                    return true;
            }

            return SyncManager.this.handleMessage(what);
        }
    };

    protected void sendMessage(final int what) {
        mHandler.sendEmptyMessage(what);
    }

    protected void sendMessage(final int what, final long delay) {
        mHandler.sendEmptyMessageDelayed(what, delay);
    }

    protected boolean handleMessage(final int what) {
        return false;
    }

    public String makeLog(String title, boolean successfully, Exception error) {
        final StringBuilder sb = new StringBuilder(title);
        if(error != null) { //ERROR OCCURED
            if(error != null) {
                sb.append("ERROR =[");
                sb.append(error.toString());
                sb.append("]");
            }
        } else {
            sb.append("[");
            sb.append("SUCCESSFUL");
            sb.append("]");
        }

        return sb.toString();
    }

    public interface OnSyncFinishedCallback {
        public void onFinished(boolean sucessfully, Exception error);
    }
}
