package com.jm.cmn.utils;

import android.content.Context;

/**
 * Created by Jakub Muran on 03.03.2017.
 */
public class SharedPreferences {

    private static android.content.SharedPreferences mPreferences;

    public static void init(Context context, String preferencesKey) {
        mPreferences = context.getSharedPreferences(preferencesKey, Context.MODE_PRIVATE);
    }

    public static android.content.SharedPreferences getDefaultPreferences() {
        return mPreferences;
    }

}
