package com.jm.cmn.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;

import com.jm.cmn.enums.AlertAction;
import com.jm.cmn.enums.AlertType;
import com.jm.cmn.interfaces.IAlertCallback;
import com.jm.cmn.models.AlertModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muran on 7/23/2015.
 */
public class AlertUtil {

    private static List<AlertDialog> dialogs = new ArrayList<>();

    public static void showListAlert(final Context context, final boolean cancelable, final String[] items, final DialogInterface.OnClickListener callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(cancelable);
        builder.setItems(items, callback);
        showDialog(builder);
    }

    public static void showListAlert(final Context context, final boolean cancelable, final String[] items, final DialogInterface.OnClickListener callback, final DialogInterface.OnDismissListener dismissCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(cancelable);
        builder.setItems(items, callback);
        showDialog(builder);
    }

    public static void showAlert(final AlertModel model, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);

        final Object title = model.title;
        final Object msg = model.message;
        final AlertType type = model.type;
        final IAlertCallback callback = model.callback;

        if(title != null) {
            if(title instanceof Integer) builder.setTitle((int) title);
            else builder.setTitle(String.valueOf(title));
        }

        if(msg != null) {
            if(msg instanceof Integer) builder.setMessage((int) msg);
            else builder.setMessage(String.valueOf(msg));
        }

        //TODO: what we will load, when we won't have resource ?
        if(type == AlertType.OK_CANCEL) {
            builder.setPositiveButton(getButtonLabelResId("Global_Alert_OKButton_Title", context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (callback != null) {
                        model.action = AlertAction.OK;
                        callback.onCloseEvent(model);
                    }
                }
            });

            builder.setNegativeButton(getButtonLabelResId("Global_Alert_CancelButton_Title", context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (callback != null) {
                        model.action = AlertAction.CANCEL;
                        callback.onCloseEvent(model);
                    }
                }
            });
        } else if(type == AlertType.OK) {
            builder.setPositiveButton(getButtonLabelResId("Global_Alert_OKButton_Title", context), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(callback != null) {
                        model.action = AlertAction.OK;
                        callback.onCloseEvent(model);
                    }
                }
            });
        } else if(type == AlertType.RELOAD) {
            builder.setPositiveButton(getButtonLabelResId("Global_Alert_ReloadButton_Title", context), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(callback != null) {
                        model.action = AlertAction.OK;
                        callback.onCloseEvent(model);
                    }
                }
            });
        }

        showDialog(builder);
    }

    protected static void showDialog(final AlertDialog.Builder builder) {
        AlertDialog alertDialog = builder.create();
        dialogs.add(alertDialog);
        alertDialog.setOnDismissListener(onDismissListener);
        alertDialog.show();
    }

    protected static int getButtonLabelResId(final String resName, final Context context) {
        final Resources res = context.getResources();
        final String packageName = context.getPackageName();
        return res.getIdentifier(resName, "string", packageName);
    }

    private static DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialogInterface) {
            try {
                dialogs.remove(dialogInterface);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}
