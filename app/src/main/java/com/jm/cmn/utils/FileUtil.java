package com.jm.cmn.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.jm.cmn.models.DownloadModel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Jakub Muran on 11/1/2017.
 */

public class FileUtil {

    private static final String TAG = "FileUtil";

    private static String filesDirPath;
    private static String tempDirPath;

    /*
    *   You can save the Bitmap to the internal storage in case the SD card is not available or
    *   for whatever other reasons you may have. Files saved to the internal storage are only
    *   accessible by the application which saved the files. Neither the user nor other
    *   applications can access those files.
    *   Also, all these files will be deleted once the application was uninstalled by the user.
     */
    public static String saveBitmapToInternalStorage(final Context context, final Bitmap bitmap, final String filename, final Bitmap.CompressFormat compressFormat) {
        try {
            final String name = filename + "." + compressFormat.toString().toLowerCase();
            FileOutputStream fos = context.openFileOutput(name, Context.MODE_PRIVATE);
            // Writing the bitmap to the output stream
            bitmap.compress(compressFormat, 100, fos);
            fos.close();
            return name;
        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());
            return null;
        }
    }

    public static void downloadFileInternally(final Context context, final DownloadModel downloadModel) throws IOException {
        //make connection
        downloadModel.setState(DownloadModel.State.WAITING);
        final URL url = new URL(downloadModel.getFilePath());
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("Authorization", downloadModel.getAuthorization());
        connection.setRequestMethod("GET");
        connection.setDoInput(true);

        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = connection.getContentLength();
        InputStream in = new BufferedInputStream(connection.getInputStream(), 1024 * 8);
        //File outputFile = new File(context.getFilesDir(), downloadModel.getFilename());
        final String dirPath = downloadModel.getDownloadedFilePath();
        File outputFile = new File(dirPath, downloadModel.getFilename());
        OutputStream out = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;

        int totalFileSize = 0;
        double current = 0d;
        int progress = 0;

        while ((count = in.read(data)) != -1) {
            downloadModel.setState(DownloadModel.State.DOWNLOADING);

            total += count;
            totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            current = Math.round(total / (Math.pow(1024, 2)));
            progress = (int) ((total * 100) / fileSize);
            long currentTime = System.currentTimeMillis() - startTime;

            downloadModel.setTotalFileSize(totalFileSize);

            if(currentTime > 1000 * timeCount) {
                downloadModel.setCurrentFileSize((int) current);
                downloadModel.setProgress(progress);
                timeCount++;
            }

            downloadModel.notifyChange();

            out.write(data, 0, count);
        }

        downloadModel.setCurrentFileSize((int) current);
        downloadModel.setProgress(progress);
        downloadModel.setDownloadedFilePath(outputFile.getAbsolutePath());
        downloadModel.setState(DownloadModel.State.FINISHED);

        out.flush();
        out.close();
        in.close();
        connection.disconnect();
    }

    public static void deleteRecursive(final String dirPath) {
        final File dirFile = new File(dirPath);
        if(dirFile.isDirectory()) {
            for (File child : dirFile.listFiles()) {
                deleteRecursive(child.getAbsolutePath());
            }
        }
        dirFile.delete();
    }

    public static String getApplicationCacheDirPath(final Context context){
        if (tempDirPath == null) {
            File externalCacheDir = context.getExternalCacheDir();

            if (externalCacheDir != null && isExternalStorageWritable()) tempDirPath = externalCacheDir.getAbsolutePath();
            else tempDirPath = context.getCacheDir().getAbsolutePath();

            (new File(tempDirPath)).mkdirs();
        }

        return tempDirPath;
    }

    public static String getInternalFilesDirPath(final Context context) {
        if(context == null) return null;
        context.getFilesDir().mkdirs();
        return context.getFilesDir().getAbsolutePath();
    }

    public static String getFilesDirPath(final Context context){
        if (filesDirPath == null) {
            File externalFilesDir = context.getExternalFilesDir(null);

            if (externalFilesDir != null && isExternalStorageWritable()) filesDirPath = externalFilesDir.getAbsolutePath();
            else filesDirPath = context.getFilesDir().getAbsolutePath();

            (new File(filesDirPath)).mkdirs();
        }

        return filesDirPath;
    }

    public static String getDirPath(final Context context, final String dirName){
        final String dirPath = getFilesDirPath(context) + "/." + dirName;
        final File dirFile = new File(dirPath);
        if(!dirFile.exists()) {
            dirFile.mkdirs();
        }
        return dirFile.getAbsolutePath();
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}
