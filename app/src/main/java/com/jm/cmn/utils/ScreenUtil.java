package com.jm.cmn.utils;

import android.content.Context;
import android.content.pm.ActivityInfo;

import com.jm.cmn.R;
import com.jm.cmn.views.activities.AbstractActivity;

/**
 * Created by Jakub Muran on 11/10/2017.
 */

public class ScreenUtil {

    public static boolean isTablet(final Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    public static boolean isPhone(final Context context) {
        return !context.getResources().getBoolean(R.bool.isTablet);
    }

    public static void allowScreenOrientationOnTablet(final Context context, final boolean allow) {
        if(isPhone(context)) return;
        if(allow) {
            ((AbstractActivity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        } else {
            ((AbstractActivity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }
    }

    public static void allowScreenOrientationOnPhone(final Context context, final boolean allow) {
        if(isTablet(context)) return;
        if(allow) {
            ((AbstractActivity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        } else {
            ((AbstractActivity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        }
    }

    public static void allowScreenOrientationOnlyOnTablet(final Context context) {
        allowScreenOrientationOnPhone(context, false);
        allowScreenOrientationOnTablet(context, true);
    }

    public static void allowScreenOrientationOnlyOnPhone(final Context context) {
        allowScreenOrientationOnPhone(context, true);
        allowScreenOrientationOnTablet(context, false);
    }

}
