package com.jm.cmn.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jakub Muran on 08.06.2016.
 */
public class DateUtil {

    public static String toUTC(final Date date) {
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return df.format(date);
    }

    public static String toUI(final Date date) {
        if(date == null) return null;
        final SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        return df.format(date);
    }

    public static String toSimpleDateUI(final Date date) {
        if(date == null) return null;
        final SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        return df.format(date);
    }
}
