package com.jm.cmn.utils;

import android.support.v4.util.PatternsCompat;

/**
 * Created by Jakub Muran on 06.11.2017.
 */
public class TextUtils {

    public static boolean validateEmail(final String email) {
        if(android.text.TextUtils.isEmpty(email)) return false;
        return PatternsCompat.EMAIL_ADDRESS.matcher(email).matches();
    }
}
