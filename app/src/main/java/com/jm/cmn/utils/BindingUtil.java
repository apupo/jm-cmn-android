package com.jm.cmn.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.databinding.BindingAdapter;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.HttpAuthHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jm.cmn.R;
import com.jm.cmn.adapters.AbstractRecyclerAdapter;
import com.jm.cmn.enums.DrawablePosition;
import com.jm.cmn.interfaces.DrawableClickListener;
import com.jm.cmn.models.elements.AbstractDatePickerFormElement;
import com.jm.cmn.transformations.GlideCircleTransformation;
import com.jm.cmn.viewmodels.AbstractTabsViewModel;
import com.jm.cmn.viewmodels.ProgressBarViewModel;
import com.jm.cmn.views.activities.AbstractActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Jakub Muran on 14.02.2017.
 */
public class BindingUtil {

    @BindingAdapter("date")
    public static void date(TextView textView, Date date) {
        textView.setText(DateUtil.toSimpleDateUI(date));
    }

    @BindingAdapter("android:layout_width")
    public static void setLayoutWidth(View view, int width) {
        final ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        view.setLayoutParams(params);
    }

    @BindingAdapter("android:layout_height")
    public static void setLayoutHeight(View view, int height) {
        final ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = height;
        view.setLayoutParams(params);
    }

    @BindingAdapter("app:relativeLayoutRule")
    public static void setRelativeLayoutRule(View view, int rule) {
        if(view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
            params.addRule(rule, view.getId());
        }
    }

    @BindingAdapter(value = {"load", "progressBarViewModel", "authName", "authPass"}, requireAll = false)
    public static void loadWebViewUrl(final WebView webView, final String url, final ProgressBarViewModel progressBarViewModel, final String authName, final String authPass) {
        if(TextUtils.isEmpty(url)) return;
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(progressBarViewModel != null) progressBarViewModel.setLoading(false);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if(progressBarViewModel != null) progressBarViewModel.setLoading(false);
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
                if(!TextUtils.isEmpty(authName) && !TextUtils.isEmpty(authPass)) {
                    handler.proceed("demo", "ohng5Sai");
                } else {
                    super.onReceivedHttpAuthRequest(view, handler, host, realm);
                }

            }
        });
        if(progressBarViewModel != null) progressBarViewModel.setLoading(true);
        webView.loadUrl(url);
    }

    @BindingAdapter("android:background")
    public static void setBackground(View view, int resId) {
        view.setBackgroundResource(resId);
    }

    @BindingAdapter("layout_height")
    public static void height(ViewGroup viewGroup, int height) {
        viewGroup.getLayoutParams().height = MathUtil.dpToPx(height);
        viewGroup.requestLayout();
    }

    @BindingAdapter("hideSoftKeyboard")
    public static void hideSoftKeyboard(EditText editText, boolean hide) {
        if(hide) {
            final AbstractActivity activity = (AbstractActivity) editText.getContext();
            editText.requestFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromInputMethod(editText.getWindowToken(), 0);
            editText.onEditorAction(EditorInfo.IME_ACTION_DONE);
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    @BindingAdapter("android:text")
    public static void text(TextView textView, int resID) {
        if(resID <= 0) return;
        textView.setText(resID);
    }

    @BindingAdapter("android:fontFamily")
    public static void textViewfontFamily(TextView textView, String fontFamily) {
        final AssetManager am = textView.getContext().getApplicationContext().getAssets();
        final Typeface typeface = Typeface.createFromAsset(am, "fonts/" + fontFamily);
        textView.setTypeface(typeface);
    }

    @BindingAdapter("android:background")
    public static void viewBackground(View view, int background) {
        view.setBackgroundResource(background);
    }

    @BindingAdapter("android:tint")
    public static void imageViewTint(ImageView imageView, int tintColorRes) {
        if(tintColorRes <= 0) return;
        imageView.setColorFilter(ContextCompat.getColor(imageView.getContext(), tintColorRes), PorterDuff.Mode.SRC_IN);
    }

    @BindingAdapter("android:textColor")
    public static void textViewColor(TextView textView, int textColorRes) {
        if(textColorRes <= 0) return;
        final int color = ContextCompat.getColor(textView.getContext(), textColorRes);
        textView.setTextColor(color);
        textView.setHintTextColor(color);
    }

    @BindingAdapter("drawableClickListener")
    public static void editTextDrawableClickListener(final EditText editText, final DrawableClickListener drawableClickListener) {
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                final Drawable drawableRight = editText.getCompoundDrawables()[DRAWABLE_RIGHT];

                if(drawableRight == null) return false;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (editText.getRight() - drawableRight.getBounds().width())) {
                        drawableClickListener.onDrawableClick(DrawablePosition.RIGHT);
                        return true;
                    }
                }
                return false;            }
        });
    }

    @BindingAdapter("android:hint")
    public static void hint(EditText editText, int resID) {
        //editText.setBackgroundResource(R.drawable.edittext_hint_border);
        if(resID <= 0) return;
        editText.setHint(resID);
    }

    @BindingAdapter("android:entries")
    public static void entries(Spinner spinner, List<String> source) {
        if(source == null) return;
        if(source.isEmpty()) return;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(spinner.getContext(), R.layout.spinner_item_layout, source);
        spinner.setAdapter(adapter);
    }

    @BindingAdapter("android:prompt")
    public static void promt(Spinner spinner, int resID) {
        if(resID <= 0) return;
        spinner.setPromptId(resID);
    }

    @BindingAdapter("onItemSelectedListener")
    public static void listener(Spinner spinner, AdapterView.OnItemSelectedListener listener) {
        if(listener == null) return;
        spinner.setOnItemSelectedListener(listener);
    }

    @BindingAdapter("selection")
    public static void listener(Spinner spinner, int position) {
        spinner.setSelection(position);
    }

    @BindingAdapter("date")
    public static void date(DatePicker datePicker, Date date) {
        if(date == null) return;
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePicker.updateDate(year, month, day);
    }

    @BindingAdapter("init")
    public static void init(DatePicker datePicker, AbstractDatePickerFormElement datePickerFormElement) {
        if(datePickerFormElement == null) return;
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(datePickerFormElement.getDate());
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePicker.init(year, month, day, datePickerFormElement);
    }

    @BindingAdapter("android:drawableRight")
    public static void addDrawableRightToTheText(final TextView textView, int drawableResID) {
        if(drawableResID == 0) {
            textView.setCompoundDrawables(null, null, null, null);
            return;
        }
        final Drawable right = ContextCompat.getDrawable(textView.getContext(), drawableResID);
        right.setBounds(0, 0, right.getIntrinsicWidth(), right.getIntrinsicHeight());
        textView.setCompoundDrawables(null, null, right, null);
    }

    @BindingAdapter("invalidMsg")
    public static void invalidMsg(final TextInputLayout textInputLayout, int invalidMsgResID) {
        if (invalidMsgResID == 0) {
            textInputLayout.clearFocus();
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
            return;
        }

        final Context context = textInputLayout.getContext();
        final String message = context.getResources().getString(invalidMsgResID);

        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(message);
        textInputLayout.requestFocus();
    }


    @BindingAdapter(value = {"invalidMsg", "invalidIcon"}, requireAll = false)
    public static void invalidMsg(final EditText editText, int invalidMsgResID, int iconDrawableId) {
        if(invalidMsgResID == 0) {
            editText.clearFocus();
            editText.setError(null);
            return;
        }

        final Context context = editText.getContext();
        final String message = context.getResources().getString(invalidMsgResID);

        editText.requestFocus();
        if(iconDrawableId == 0) {
            editText.setError(message);
        } else {
            final Drawable drawable = ContextCompat.getDrawable(context, iconDrawableId);
            editText.setError(message, drawable);
        }

/*
        ViewParent parent = editText.getParent();
        while (!(parent instanceof ScrollView)) {
            parent = parent.getParent();
        }

        if(parent != null && parent instanceof ScrollView) {
            final ScrollView scrollView = (ScrollView) parent;
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, editText.getBottom());
                }
            });
        }
*/
    }

    @BindingAdapter("invalidMsg")
    public static void invalidMsg(final TextView textView, int invalidMsgResID) {
        if(invalidMsgResID == 0) {
            textView.clearFocus();
            textView.setError(null);
            return;
        }
        textView.requestFocus();
        //textView.setError(textView.getContext().getResources().getString(invalidMsgResID));
        textView.setText(textView.getContext().getResources().getString(invalidMsgResID));

        ViewParent parent = textView.getParent();
        while (!(parent instanceof ScrollView)) {
            parent = parent.getParent();
        }

        if(parent != null && parent instanceof ScrollView) {
            final ScrollView scrollView = (ScrollView) parent;
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, textView.getBottom());
                }
            });
        }
    }

    @BindingAdapter(value = {"orientation", "spanCount"}, requireAll = false)
    public static void orientation(RecyclerView recyclerView, int orientation, int spanCount) {
        RecyclerView.LayoutManager lm = null;
        if(orientation >= 0) {
            lm = new LinearLayoutManager(recyclerView.getContext());
            ((LinearLayoutManager)lm).setOrientation(orientation);
        } else if(spanCount > 0) {
            lm = new GridLayoutManager(recyclerView.getContext(), spanCount);
        }
        recyclerView.setLayoutManager(lm);
    }

    @BindingAdapter("adapter")
    public static void adapter(RecyclerView recyclerView, AbstractRecyclerAdapter adapter) {
        if(adapter == null) return;
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("init")
    public static void initTabs(TabHost tabHost, AbstractTabsViewModel tabsViewModel) {
        tabHost.setup();
        tabsViewModel.setTabHost(tabHost);
        tabsViewModel.addTabs(tabsViewModel.getSaveInstanceState());
        tabHost.setOnTabChangedListener(tabsViewModel);
        tabHost.getTabWidget().setDividerDrawable(R.drawable.tab_separator_shape);
    }

    @BindingAdapter(value = {"adapter", "onPageChangeListener"}, requireAll = false)
    public static void setViewPagerAdapter(ViewPager viewPager, PagerAdapter adapter, ViewPager.OnPageChangeListener listener) {
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(listener);
    }

    @BindingAdapter("onPageChanged")
    public static void onPageSelected(ViewPager viewPager, int page) {
        viewPager.setCurrentItem(page);
    }

    @BindingAdapter("android:src")
    public static void setImageSrcRes(ImageView imageView, int resId) {
        imageView.setImageResource(resId);
    }

    @BindingAdapter(value = {"src", "scaleType", "circle"}, requireAll = false)
    public static void setSrc(ImageView imageView, String url, ImageView.ScaleType scaleType, boolean circle) {
        if(url == null) {
            imageView.setVisibility(View.INVISIBLE);
            return;
        }

        imageView.setVisibility(View.VISIBLE);

        if(scaleType != null) imageView.setScaleType(scaleType);

        Glide.with(imageView.getContext())
                .load(url)
                .asBitmap()
                .fitCenter()
                .transform(circle ? new GlideCircleTransformation(imageView.getContext()) : null)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }
}
