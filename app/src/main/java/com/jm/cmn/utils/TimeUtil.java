package com.jm.cmn.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jakub Muran on 10/27/2017.
 */

public class TimeUtil {

    public static class DiffTime {
        public static enum Type {
            MILLIS, SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS;
        }

        private Type mType;
        public Type getType() { return this.mType; }

        private String mMessage;
        public String getMessage() { return this.mMessage; }

        private long mValue;
        public long getValue() { return this.mValue; }

        public DiffTime(Type mType, String mMessage, long mValue) {
            this.mType = mType;
            this.mMessage = mMessage;
            this.mValue = mValue;
        }
    }

    private static long SECONDS = 1000;
    private static long MINUTES = SECONDS * 60l;
    private static long HOURS = MINUTES * 60l;
    private static long DAYS = HOURS * 24l;
    private static long WEEKS = DAYS * 7l;
    private static long MONTHS = DAYS * 30l;
    private static long YEARS = DAYS * 365l;

    public static DiffTime getDiffTime(final Date date) {
        final long diffMillis = date.getTime() - new Date().getTime();

        final long diffSeconds = diffMillis / SECONDS;
        final long diffMinutes = diffMillis / MINUTES;
        final long diffHours = diffMillis / HOURS;
        final long diffDays = diffMillis / DAYS;
        final long diffWeeks = diffMillis / WEEKS;
        final long diffMonths = diffMillis / MONTHS;
        final long diffYears = diffMillis / YEARS;

        String msg = "";
        DiffTime.Type type = null;
        long diffValue = 0l;

        if(diffSeconds < 1) {
            type = DiffTime.Type.MILLIS;
            msg = "one sec ago";
            diffValue = diffSeconds;
        } else if(diffMinutes < 1) {
            type = DiffTime.Type.SECONDS;
            msg = diffSeconds <= 1 ? diffSeconds + " " + "second ago" : diffSeconds + " " + "seconds ago";
            diffValue = diffSeconds;
        } else if(diffHours < 1) {
            type = DiffTime.Type.MINUTES;
            msg = diffMinutes <= 1 ? diffMinutes + " " + "minute ago" : diffMinutes + " " + "minutes ago";
            diffValue = diffMinutes;
        } else if(diffDays < 1) {
            type = DiffTime.Type.HOURS;
            msg = diffHours <= 1 ? diffHours + " " + "hour ago" : diffHours + " " + "hours ago";
            diffValue = diffHours;
        } else if(diffWeeks < 1) {
            type = DiffTime.Type.DAYS;
            msg = diffDays <= 1 ? diffDays + " " + "day ago" : diffDays + " " + "days ago";
            diffValue = diffDays;
        } else if(diffMonths < 1) {
            type = DiffTime.Type.WEEKS;
            msg = diffWeeks <= 1 ? diffWeeks + " " + "week ago" : diffWeeks + " " + "weeks ago";
            diffValue = diffWeeks;
        } else if(diffYears < 12) {
            type = DiffTime.Type.MONTHS;
            msg = diffMonths <= 12 ? diffMonths + " " + "month ago" : diffMonths + " " + "months ago";
            diffValue = diffMonths;
        } else {
            type = DiffTime.Type.YEARS;
            msg = diffYears <= 1 ? diffYears + " " + "year ago" : diffYears + " " + "years ago";
            diffValue = diffYears;
        }

        return new DiffTime(type, msg, diffValue);
    }

}
