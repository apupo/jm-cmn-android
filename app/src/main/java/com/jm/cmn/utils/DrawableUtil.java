package com.jm.cmn.utils;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;

/**
 * Created by Jakub Muran on 31.05.2016.
 */
public class DrawableUtil {

    // a hack for coloring edge glow colors
    public static boolean applySupportEdgeGlowColors(final Resources resources, final int glowColor) {
        if (Build.VERSION.SDK_INT < 20) {
            try {
                //glow
                int glowDrawableId = resources.getIdentifier("overscroll_glow", "drawable", "android");
                Drawable androidGlow = resources.getDrawable(glowDrawableId);
                androidGlow.setColorFilter(glowColor, PorterDuff.Mode.SRC_IN);
                //edge
                int edgeDrawableId = resources.getIdentifier("overscroll_edge", "drawable", "android");
                Drawable androidEdge = resources.getDrawable(edgeDrawableId);
                androidEdge.setColorFilter(glowColor, PorterDuff.Mode.SRC_IN);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public static int normalColor = 0x000000;

    public static void tintDrawable(final Drawable drawable) {
        tintDrawable(drawable, normalColor);
    }

    public static void tintDrawable(final Drawable drawable, final int color) {
        if(drawable == null) return;

        drawable.mutate();

        final ColorFilter cf = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN);
        drawable.setColorFilter(cf);
    }

}
