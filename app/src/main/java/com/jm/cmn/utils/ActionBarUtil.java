package com.jm.cmn.utils;

import android.content.Context;
import android.support.v7.app.ActionBar;

import com.jm.cmn.views.activities.AbstractActivity;

/**
 * Created by Jakub Muran on 13.01.2017.
 */
public class ActionBarUtil {
        public static ActionBar changeActionBarVisibility(Context context, boolean visible) {
        ActionBar actionBar = null;
        if(context != null && context instanceof AbstractActivity) {
            final AbstractActivity abstractActivity = (AbstractActivity) context;
            actionBar = abstractActivity.getSupportActionBar();
            if(actionBar != null) {
                if(visible) {
                    actionBar.show();
                } else {
                    actionBar.hide();
                }
            }
        }

        return actionBar;
    }
}
