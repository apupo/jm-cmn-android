package com.jm.cmn.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.jm.cmn.interfaces.IObtainFileCallback;
import com.jm.cmn.interfaces.IObtainFileObserver;
import com.jm.cmn.views.activities.AbstractActivity;

/**
 * Created by Jakub Muran on 16.10.2017.
 */

public class TakeFileUtil {

    public static final int TAKE_FILE_REQUEST_CODE = 101;
    public static final int PERMISSION_READ_EXTERNAL_STORAGE_REQUEST_CODE = 501;

    private static IObtainFileObserver mLastObserver;
    private static IObtainFileCallback mLastCallback;

    public static TakeFileModel makeObtainFileModel() {
        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        fileIntent.setType("file/*");
        fileIntent.setAction(Intent.ACTION_GET_CONTENT);
        fileIntent.addCategory(Intent.CATEGORY_OPENABLE);
        fileIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        return new TakeFileModel(fileIntent);
    }

    public static void permissionGrantedResult(boolean granted) {
        if(!granted) return;
        if(mLastObserver == null) return;
        mLastObserver.obtainFile(mLastCallback);
        mLastObserver = null;
        mLastCallback = null;
    }

    public static boolean isPermissionGranted(Context context, IObtainFileObserver observer, IObtainFileCallback callback) {
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AbstractActivity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_READ_EXTERNAL_STORAGE_REQUEST_CODE);
            mLastObserver = observer;
            mLastCallback = callback;
            return false;
        } else {
            return true;
        }
    }

    public static class TakeFileModel {
        public Intent requestIntent;

        public TakeFileModel(Intent requestIntent) {
            this.requestIntent = requestIntent;
        }
    }

}

