package com.jm.cmn.utils;

import android.location.Location;

/**
 * Created by Jakub Muran on 06.03.2017.
 */
public class IntechLocationUtil {

    public static Location toLocation(String str) {
        if(str == null) return null;
        str = str.trim();
        String[] arr = str.split(",");
        Location location = null;
        if(arr != null && arr.length == 2) {
            final String lat = arr[0];
            final String lon = arr[1];
            location = new Location("dummyprovider");
            location.setLatitude(Double.valueOf(lat));
            location.setLongitude(Double.valueOf(lon));
        }
        return location;
    }

    public static String toString(Location location) {
        if(location == null) return null;
        return location.getLatitude() + "," + location.getLongitude();
    }
}
