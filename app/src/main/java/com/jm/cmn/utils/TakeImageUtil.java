package com.jm.cmn.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.jm.cmn.R;
import com.jm.cmn.interfaces.ICameraCallback;
import com.jm.cmn.interfaces.ICameraView;
import com.jm.cmn.views.activities.AbstractActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by muran on 7/29/2015.
 */
public class TakeImageUtil {

    public static final int TAKE_IMAGE_REQUEST_CODE = 100;
    public static final int PERMISSION_WIRTE_EXTERNAL_STORAGE_REQUEST_CODE = 111;

    private static ICameraView mLastCameraViewObserver;
    private static ICameraCallback mLastCameraCallback;

    public static TakeImageModel obtainImageFromCameraOrDocuments() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,null);
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);

        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, R.string.Chooser_Image_Intent_Title);

        Intent[] intentArray =  {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

        return new TakeImageModel(chooser, photoFile);
    }

    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public static void writePermissionGrantedResult(boolean granted) {
        if(!granted) return;
        if(mLastCameraViewObserver == null) return;
        if(mLastCameraCallback == null) return;
        mLastCameraViewObserver.obtainPhoto(mLastCameraCallback);
        mLastCameraViewObserver = null;
        mLastCameraCallback = null;
    }

    public static boolean isWritePermissionGranted(Context context, ICameraView cameraView, ICameraCallback callback) {
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AbstractActivity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WIRTE_EXTERNAL_STORAGE_REQUEST_CODE);
            mLastCameraViewObserver = cameraView;
            mLastCameraCallback = callback;
            return false;
        } else {
            return true;
        }
    }

    public static boolean isCameraSupported(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public static class TakeImageModel {
        public Intent requestIntent;
        public File imageFile;

        public TakeImageModel(Intent requestIntent, File imageFile) {
            this.requestIntent = requestIntent;
            this.imageFile = imageFile;
        }
    }

}
