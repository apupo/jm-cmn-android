package com.jm.cmn.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;

import com.jm.cmn.R;

/**
 * Created by Jakub Muran on 05.10.2017.
 */

public class MenuUtil {

    public static void setMenuItemHighlight(Context context, MenuItem item, boolean highlight) {
        if(highlight) {
            DrawableUtil.tintDrawable(item.getIcon(), ContextCompat.getColor(context, R.color.action_bar_icon_selected_color));
        } else {
            DrawableUtil.tintDrawable(item.getIcon(), ContextCompat.getColor(context, R.color.action_bar_icon_normal_color));
        }
    }

}
